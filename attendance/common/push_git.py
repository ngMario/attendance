# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/11/12 16:59
# @File    : push_git.py
from __future__ import unicode_literals

import subprocess

from django.conf import settings


def execute_cmd(cmd):
    p = subprocess.Popen(cmd,
                         shell=True,
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if p.returncode != 0:
        return p.returncode, stderr
    return p.returncode, stdout


def push_git(cmds):
    # cmds = 'copy E:\code\python\project\\attendance\\attendance\\report\\file\运管平台项目周报-刘安森4.xlsx  E:\code\python\project\docsx\\report'
    return_code, out = execute_cmd(cmds)
    print(return_code, out)
    if return_code != 0:
        raise SystemExit('execute {0} err :{1}'.format(cmds, out))
    else:
        print("execute command ({0} sucessful)".format(cmds))
    cmds2 = 'sh {0}'.format(settings.SSH_FILE_URL)
    return_code2, out2 = execute_cmd(cmds2)
    print(return_code2, out2)
    if return_code2 != 0:
        raise SystemExit('execute {0} err :{1}'.format(cmds2, out2))
    else:
        print("execute command ({0} sucessful)".format(cmds2))


# if __name__ == '__main__':
#     push_git()