# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/7 15:13
# @File    : api_urls.py
from __future__ import unicode_literals

from sundry import api
from attendance.router import router

router.register(r'record', api.WithdrawalRecordViewSet, 'with_record')

urlpatterns = [
]
