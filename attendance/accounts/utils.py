# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/12/2 14:40
# @File    : utils.py
from __future__ import unicode_literals

from datetime import date, datetime
from dateutil.relativedelta import relativedelta


def return_week_date():
    """
    获取当前周的周一日期
    :return:
    """
    week_day = date.today()
    if week_day.isoweekday() == 1:
        return week_day
    if week_day.isoweekday() == 2:
        return week_day - relativedelta(days=1)
    if week_day.isoweekday() == 3:
        return week_day - relativedelta(days=2)
    if week_day.isoweekday() == 4:
        return week_day - relativedelta(days=3)
    if week_day.isoweekday() == 5:
        return week_day - relativedelta(days=4)
    if week_day.isoweekday() == 6:
        return week_day - relativedelta(days=5)
    if week_day.isoweekday() == 7:
        return week_day - relativedelta(days=6)
    return 0


def return_week_five_date():
    """
    获取当前周的周五日期
    :return:
    """
    week_day = date.today()
    if week_day.isoweekday() == 1:
        return week_day+relativedelta(days=4)
    if week_day.isoweekday() == 2:
        return week_day + relativedelta(days=3)
    if week_day.isoweekday() == 3:
        return week_day + relativedelta(days=2)
    if week_day.isoweekday() == 4:
        return week_day + relativedelta(days=1)
    if week_day.isoweekday() == 5:
        return week_day
    if week_day.isoweekday() == 6:
        return week_day - relativedelta(days=1)
    if week_day.isoweekday() == 7:
        return week_day - relativedelta(days=2)
    return 0


def return_next_week_date():
    week_day = date.today()
    if week_day.isoweekday() == 1:
        return week_day + relativedelta(days=7)
    if week_day.isoweekday() == 2:
        return week_day + relativedelta(days=6)
    if week_day.isoweekday() == 3:
        return week_day + relativedelta(days=5)
    if week_day.isoweekday() == 4:
        return week_day + relativedelta(days=4)
    if week_day.isoweekday() == 5:
        return week_day + relativedelta(days=3)
    if week_day.isoweekday() == 6:
        return week_day + relativedelta(days=2)
    if week_day.isoweekday() == 7:
        return week_day + relativedelta(days=1)
    return 0


def return_next_five_week_date():
    week_day = date.today()
    if week_day.isoweekday() == 1:
        return week_day + relativedelta(days=11)
    if week_day.isoweekday() == 2:
        return week_day + relativedelta(days=10)
    if week_day.isoweekday() == 3:
        return week_day + relativedelta(days=9)
    if week_day.isoweekday() == 4:
        return week_day + relativedelta(days=8)
    if week_day.isoweekday() == 5:
        return week_day + relativedelta(days=7)
    if week_day.isoweekday() == 6:
        return week_day + relativedelta(days=6)
    if week_day.isoweekday() == 7:
        return week_day + relativedelta(days=5)
    return 0