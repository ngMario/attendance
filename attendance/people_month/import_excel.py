# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/8 17:21
# @File    : import_excel.py
from __future__ import unicode_literals

from collections import OrderedDict
from import_export import resources, fields
from import_export.admin import ImportMixin, ExportMixin, ImportExportMixin
from import_export.widgets import ManyToManyWidget, ForeignKeyWidget
from import_export.results import Result, RowResult
from django.db import IntegrityError

from .utils import *
from .models import *


class TheJobInfoResource(resources.ModelResource):
    people = fields.Field(column_name='姓名', attribute='姓名')
    jobs_day = fields.Field(column_name='在岗天数', attribute='在岗天数')
    info_project_name = fields.Field(column_name='合同项目', attribute='合同项目')
    team = fields.Field(column_name='团队', attribute='团队')
    created_at = fields.Field(column_name='创建时间', attribute='创建时间')

    class People:
        model = TheJobInfo
        fields = ()
        import_id_fields = ('people', 'jobs_day', 'info_project_name', 'team', 'created_at')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        for row in dataset.dict:
            error = ""
            people_name = row['姓名']
            jobs_day = row['在岗天数']
            info_project_name = row['合同项目']
            team_name = row['团队']
            created_at = row['创建时间']

            try:
                people = People.objects.get(name=people_name)
            except People.DoesNotExist:
                error += '人员{0}不存在！'.format(str(id))
                return result

            try:
                contract_project = ContractProject.objects.get(project_name=info_project_name)
            except ContractProject.DoesNotExist:
                error += '合同项目{0}不存在！'.format(str(info_project_name))
                return result

            try:
                team = Team.objects.get(team_name=team_name)
            except Team.DoesNotExist:
                error += '团队{0}不存在！'.format(str(id))
                return result

            try:
                TheJobInfo.objects.get(people=people, info_project_name=contract_project,
                    team=team, created_at=created_at)
                error += "当前月份当前合同人员在岗天数数据已存在"
                return result
            except TheJobInfo.DoesNotExist:
                try:
                    job_info, created = TheJobInfo.objects.get_or_create(
                        people=people, info_project_name=contract_project,
                        team=team,
                        defaults={
                            'jobs_day': jobs_day,
                            'created_at': created_at
                        }
                    )
                    if not created:
                        job_info.jobs_day = jobs_day
                        job_info.modified_at = datetime.now()
                        job_info.created_at = created_at
                        job_info.save(update_fields=[
                            'jobs_day', 'created_at', 'modified_at'])
                except IntegrityError:
                    return result
        return result


class PeopleContractProjectResource(resources.ModelResource):
    people = fields.Field(column_name='姓名', attribute='姓名')
    contract_project = fields.Field(column_name='合同项目', attribute='合同项目')
    start_time = fields.Field(column_name='投入时间', attribute='投入时间')
    end_time = fields.Field(column_name='结束时间', attribute='结束时间')
    is_start = fields.Field(column_name='人员是否结束投入', attribute='人员是否结束投入')

    class People:
        model = PeopleContractProject
        fields = ()
        import_id_fields = ('people', 'contract_project', 'start_time', 'end_time', 'is_start')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        for row in dataset.dict:
            error = ""
            people_name = row['姓名']
            contract_project_name = row['合同项目']
            start_time = row['投入时间']
            end_time = row['结束时间']
            is_start_txt = row['人员是否结束投入']
            try:
                people = People.objects.get(name=people_name)
            except People.DoesNotExist:
                error += '人员{0}不存在！'.format(str(people_name))
                return result

            try:
                contract_project = ContractProject.objects.get(project_name=contract_project_name)
            except ContractProject.DoesNotExist:
                error += '合同项目{0}不存在！'.format(str(contract_project_name))
                return result
            if is_start_txt == '是':
                is_start = True
            else:
                is_start = False

            try:
                contract, created = PeopleContractProject.objects.get_or_create(
                    people=people, contract_project=contract_project, defaults={
                        'start_time': start_time,
                        'end_time': end_time,
                        'is_start': is_start
                    }
                )
                if not created:
                    contract.start_time = start_time
                    contract.end_time = end_time
                    contract.is_start = is_start
                    contract.save(update_fields=[
                        'people', 'contract_project', 'start_time', 'end_time', 'is_start'])
            except IntegrityError:
                return result

        return result


class PeopleResource(resources.ModelResource):
    name = fields.Field(column_name='姓名', attribute='姓名')
    company_name = fields.Field(column_name='供应商公司', attribute='供应商公司')
    mobile = fields.Field(column_name='联系方式', attribute='联系方式')
    email = fields.Field(column_name='邮箱', attribute='邮箱')
    entry_time = fields.Field(column_name='入职时间', attribute='入职时间')
    graduation_time = fields.Field(column_name='毕业时间', attribute='毕业时间')
    direction = fields.Field(column_name='专业方向', attribute='专业方向')
    schooling = fields.Field(column_name='学历', attribute='学历')
    admission_qualification = fields.Field(column_name='进场级别', attribute='进场级别')
    state = fields.Field(column_name='在职状态', attribute='在职状态')
    resign_date = fields.Field(column_name='离职时间', attribute='离职时间')
    responsibility = fields.Field(column_name='工作职责', attribute='工作职责')
    note = fields.Field(column_name='备注', attribute='备注')

    class People:
        model = ContractProject
        fields = ()
        import_id_fields = ('name', 'company_name', 'mobile', 'email', 'entry_time', 'graduation_time',
                            'direction', 'schooling', 'admission_qualification', 'state', 'resign_date',
                            'responsibility', 'note')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        for row in dataset.dict:
            error = ""
            name = row['姓名']
            company_name = row['供应商公司']
            mobile = row['联系方式']
            email = row['邮箱']
            entry_time = row['入职时间']
            graduation_time = row['毕业时间']
            direction = row['专业方向']
            schooling_txt = row['学历']
            admission = row['进场级别']
            state_txt = row['在职状态']
            resign_date = row['离职时间']
            responsibility = row['工作职责']
            note = row['备注']

            if graduation_time is None or graduation_time is None:
                return "毕业时间或入职时间不能为空"

            try:
                company = Company.objects.get(name=company_name)
            except Company.DoesNotExist:
                error += '供应商公司{0}不存在！'.format(str(id))
                return result

            if schooling_txt == '大专':
                schooling = 0
            elif schooling_txt == '本科':
                schooling = 1
            elif schooling_txt == '硕士':
                schooling = 2
            elif schooling_txt == '博士':
                schooling = 3
            elif schooling_txt == '博士后':
                schooling = 4
            else:
                schooling = 5

            if admission == '初级工程师':
                admission_qualification = 0
            elif admission == '中级工程师':
                admission_qualification = 1
            elif admission == '高级工程师':
                admission_qualification = 2
            elif admission == '专家':
                admission_qualification = 3
            elif admission == '资深专家':
                admission_qualification = 4
            else:
                admission_qualification = 5

            if state_txt == '在职':
                state = 0
            elif state_txt == '休假':
                state = 2
            else:
                state = 1

            try:
                people, created = People.objects.get_or_create(
                    name=name, defaults={
                        'company_name': company,
                        'mobile': mobile,
                        'email': email,
                        'entry_time': entry_time,
                        'graduation_time': graduation_time,
                        'direction': direction,
                        'schooling': schooling,
                        'admission_qualification': admission_qualification,
                        'state': state,
                        'resign_date': resign_date,
                        'responsibility': responsibility,
                        'note': note,
                    }
                )
                if not created:
                    people.company_name = company
                    people.mobile = mobile
                    people.email = email
                    people.entry_time = entry_time
                    people.graduation_time = graduation_time
                    people.direction = direction
                    people.schooling = schooling
                    people.admission_qualification = admission_qualification
                    people.state = state
                    people.resign_date = resign_date
                    people.responsibility = responsibility
                    people.note = note
                    people.save(update_fields=[
                        'mobile', 'email', 'entry_time', 'graduation_time', 'direction', 'schooling',
                        'admission_qualification', 'state', 'company_name', 'name',
                        'resign_date', 'responsibility', 'note', 'modified_at'])
            except IntegrityError:
                return result

        return result


class ContractProjectResource(resources.ModelResource):
    team_name = fields.Field(column_name='团队名称', attribute='团队名称')
    project_name = fields.Field(column_name='合同项目名称', attribute='合同项目名称')
    project_note = fields.Field(column_name='合同项目描述', attribute='合同项目描述')
    project_type = fields.Field(column_name='合同项目状态', attribute='合同项目状态')
    bank_head = fields.Field(column_name='行方负责人', attribute='行方负责人')
    company_head = fields.Field(column_name='公司负责人', attribute='公司负责人')
    cycle = fields.Field(column_name='合同周期', attribute='合同周期')
    notice_time = fields.Field(column_name='项目入场通知时间', attribute='项目入场通知时间')
    start_time = fields.Field(column_name='项目入场时间', attribute='项目入场时间')
    contract_month = fields.Field(column_name='合同总人月数', attribute='合同总人月数')
    senior_month = fields.Field(column_name='资深专家合同人月数', attribute='资深专家合同人月数')
    experts_month = fields.Field(column_name='专家合同人月数', attribute='专家合同人月数')
    high_month = fields.Field(column_name='高级工程师合同人月数', attribute='高级工程师合同人月数')
    intermediate_month = fields.Field(column_name='中级工程师合同人月数', attribute='中级工程师合同人月数')
    primary_month = fields.Field(column_name='初级工程师合同人月数', attribute='初级工程师合同人月数')
    end_time = fields.Field(column_name='项目结束时间', attribute='项目结束时间')

    class Meta:
        model = ContractProject
        fields = ()
        import_id_fields = ('team_name', 'project_name', 'project_note',
                            'project_type', 'bank_head', 'company_head', 'cycle', 'notice_time',
                            'start_time', 'contract_month', 'senior_month', 'experts_month',
                            'high_month', 'intermediate_month', 'primary_month', 'end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        for row in dataset.dict:
            error = ""
            team_name = row['团队名称']
            project_name = row['合同项目名称']
            project_note = row['合同项目描述']
            type = row['合同项目状态']
            bank_head = row['行方负责人']
            company_head = row['公司负责人']
            cycle = row['合同周期']
            notice_time = row['项目入场通知时间']
            start_time = row['项目入场时间']
            contract_month = row['合同总人月数']
            senior_month = row['资深专家合同人月数']
            experts_month = row['专家合同人月数']
            high_month = row['高级工程师合同人月数']
            intermediate_month = row['中级工程师合同人月数']
            primary_month = row['初级工程师合同人月数']
            end_time = row['项目结束时间']
            if type == '未开始':
                project_type = 0
            elif type == '已开始':
                project_type = 1
            elif type == '结束':
                project_type = 2
            else:
                project_type = 3
            try:
                team = Team.objects.get(team_name=team_name)
            except Team.DoesNotExist:
                error += '团队{0}不存在！'.format(str(id))
                return result
            try:
                project, created = ContractProject.objects.get_or_create(
                    team=team, project_name=project_name, defaults={
                        'project_note': project_note,
                        'project_type': project_type,
                        'bank_head': bank_head,
                        'company_head': company_head,
                        'cycle': cycle,
                        'notice_time': notice_time,
                        'start_time': start_time,
                        'contract_month': contract_month,
                        'senior_month': senior_month,
                        'experts_month': experts_month,
                        'high_month': high_month,
                        'intermediate_month': intermediate_month,
                        'primary_month': primary_month,
                        'end_time': end_time
                    }
                )
                if not created:
                    project.project_note = project_note
                    project.project_type = project_type
                    project.bank_head = bank_head
                    project.company_head = company_head
                    project.cycle = cycle
                    project.notice_time = notice_time
                    project.start_time = start_time
                    project.contract_month = contract_month
                    project.senior_month = senior_month
                    project.experts_month = experts_month
                    project.high_month = high_month
                    project.intermediate_month = intermediate_month
                    project.primary_month = primary_month
                    project.end_time = end_time
                    project.save(update_fields=[
                        'project_name', 'project_note', 'project_type', 'bank_head', 'company_head', 'cycle', 'notice_time',
                        'start_time', 'contract_month', 'senior_month', 'experts_month', 'high_month', 'intermediate_month',
                        'primary_month', 'end_time'])
            except IntegrityError:
                return result
        return result


class TeamResource(resources.ModelResource):
    team_name = fields.Field(column_name='团队名称', attribute='团队名称')
    team_note = fields.Field(column_name='团队描述', attribute='团队描述')
    team_head = fields.Field(column_name='团队负责人', attribute='团队负责人')
    team_type = fields.Field(column_name='团队状态', attribute='团队状态')
    start_time = fields.Field(column_name='开始时间', attribute='开始时间')
    end_time = fields.Field(column_name='结束时间', attribute='结束时间')

    class Meta:
        model = Team
        fields = ()
        import_id_fields = ('team_name', 'team_note', 'team_head',
                            'team_type', 'start_time', 'end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        for row in dataset.dict:
            error = ''
            team_name = row['团队名称']
            team_note = row['团队描述']
            team_head = row['团队负责人']
            type = row['团队状态']
            start_time = row['开始时间']
            end_time = row['结束时间']
            if type == '未开始':
                team_type = 0
            elif type == '已开始':
                team_type = 1
            elif type == '结束':
                team_type = 2
            else:
                team_type = 3
            try:
                team, created = Team.objects.get_or_create(
                    team_name=team_name, defaults={
                        'team_note': team_note,
                        'team_head': team_head,
                        'team_type': team_type,
                        'start_time': start_time,
                        'end_time': end_time
                    }
                )
                if not created:
                    team.team_note = team_note
                    team.team_head = team_head
                    team.team_type = team_type
                    team.start_time = start_time
                    team.end_time = end_time
                    team.save(update_fields=['team_note', 'team_head', 'team_type', 'start_time',
                                             'end_time', 'modified_at'])
            except IntegrityError:
                pass
        return result

