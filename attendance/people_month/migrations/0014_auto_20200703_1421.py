# Generated by Django 2.2.6 on 2020-07-03 06:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people_month', '0013_auto_20200702_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='people',
            name='admission_qualification',
            field=models.SmallIntegerField(choices=[(0, '初级工程师'), (1, '中级工程师'), (2, '高级工程师'), (3, '专家'), (4, '资深专家'), (5, '其他')], default=0, verbose_name='进场级别'),
        ),
    ]
