# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/10/28 15:12
# @File    : serializers.py
from __future__ import unicode_literals

from rest_framework import serializers


class AttendanceSerializer(serializers.Serializer):
    attendance_group = serializers.CharField(label='考勤系统用户名', help_text='考勤系统用户名')
    # project_group = serializers.CharField(label='项目组名称', help_text='项目组名称')
    attendance_date = serializers.CharField(label='考勤时间', help_text='考勤时间')
    in_date = serializers.CharField(label='签到时间', help_text='签到时间')
    back_date = serializers.CharField(label='签退时间', help_text='签退时间')


class UserSerializer(serializers.Serializer):
    name = serializers.CharField(label='name', help_text='name')
    username = serializers.CharField(label='username', help_text='username')
    # project_group = serializers.CharField(label='项目组名称', help_text='项目组名称')
    email = serializers.CharField(label='email', help_text='email')
    num = serializers.CharField(label='num', help_text='num')


class UserPositionSerializer(serializers.Serializer):
    name = serializers.CharField(label='name', help_text='name')
    position = serializers.CharField(label='position', help_text='position')


class RegisteredSerializer(serializers.Serializer):
    name = serializers.CharField(label='姓名', help_text='中文姓名')
    username = serializers.CharField(label='用户名', help_text='用户名')
    email = serializers.CharField(label='邮箱', help_text='填写公司邮箱，用于找回密码')
    password = serializers.CharField(label='密码', help_text='密码')
    project_name = serializers.CharField(label='项目组名称', help_text='项目组名称')
    team_type = serializers.CharField(label='所属小组', help_text='安全小组or数据小组')

