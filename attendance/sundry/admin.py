# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/3/27 15:36
# @File    : admin.py
from __future__ import unicode_literals

from django.contrib import admin

from .models import *


@admin.register(Accounts)
class AccountsAdmin(admin.ModelAdmin):
    """
    用户资料
    """
    list_display = ('id', 'name', 'email', 'id_card_no', 'mobile', 'id_card_photo_front', 'id_card_photo_back',
                    'status', 'status_note')
    # readonly_fields = ['name']
    search_fields = ('name', )
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50


@admin.register(WithdrawalRecord)
class WithdrawalRecordAdmin(admin.ModelAdmin):
    """
    用户资料
    """
    list_display = ('id', 'account', 'bank_card_no', 'bank_name', 'amount', 'status', 'note',
                    'created_at', 'modified_at')
    readonly_fields = ['account', 'bank_card_no', 'bank_name', 'amount', 'created_at', 'modified_at']
    search_fields = ('bank_card_no', )
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50

