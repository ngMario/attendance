# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/4/3 15:36
# @File    : admin.py
from __future__ import unicode_literals

from collections import OrderedDict
from django.contrib import admin
from django.db.models import Sum
from django.db import IntegrityError
from import_export.admin import ImportMixin, ExportMixin, ImportExportModelAdmin
from import_export import resources, fields
from import_export.widgets import ManyToManyWidget, ForeignKeyWidget
from import_export.results import Result, RowResult

from .models import *

class PeopleEntranceResource(resources.ModelResource):
    """人员入场情况导入函数"""
    id = fields.Field(column_name='id', attribute='id')
    people = fields.Field(column_name='people', attribute='people', widget=ForeignKeyWidget(People, 'id'))
    entrance_type = fields.Field(column_name='entrance_type', attribute='entrance_type')
    team = fields.Field(column_name='team', attribute='team', widget=ForeignKeyWidget(Team, 'id'))
    project = fields.Field(column_name='project', attribute='project', widget=ForeignKeyWidget(Project, 'id'))
    entrance_note = fields.Field(column_name='entrance_note', attribute='entrance_note')
    end_time = fields.Field(column_name='end_time', attribute='end_time')
    entrance_date = fields.Field(column_name='entrance_date', attribute='entrance_date')

    class Meta:
        model = PeopleEntrance
        fields = ()
        import_id_fields = ('id', 'people', 'entrance_type', 'team', 'project', 'entrance_note', 'end_time')
    
    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = ['id', '姓名', '入场状态', '人员所在实施团队', '人员当前入场项目', '当前项目组描述', '项目结束时间']
        # result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset) - 1
        for row in dataset.dict:
            error = ''
            id = row['id']
            name = row['姓名']
            entrance = row['入场状态']
            team_name = row['人员所在实施团队']
            project_name = row['人员当前入场项目']
            entrance_note = row['当前项目组描述']
            end_time = row['项目结束时间']
            entrance_date = row['年份日期']
            if id is None or name is None or team_name is None or project_name is None:
                continue
            try:
                name = name.strip()
                entrance = entrance.strip()
                team_name = team_name.strip()
                project_name = project_name.strip()
                entrance_note = entrance_note.strip()
            except:
                error += '值不能为空'

            if entrance == '入场':
                entrance_type = 0
            elif entrance == '未入场':
                entrance_type = 1
            elif entrance == '闲置':
                entrance_type = 2
            else:
                entrance_type = 3
            try:
                project = Project.objects.get(project_name=project_name)
            except Project.DoesNotExist:
                error += '项目{0}不存在！'.format(project_name)
            try:
                team = Team.objects.get(team_name=team_name)
            except Team.DoesNotExist:
                error += '团队{0}不存在！'.format(team_name)
            try:
                people = People.objects.get(name=name)
            except People.DoesNotExist:
                error += '当前人员{0}不存在！'.format(name)
            try:
                # 防止重复数据产生
                trance = PeopleEntrance.objects.get(people=people.id)
                if trance.id != id:
                    continue
                try:
                    en_list, created = PeopleEntranceList.objects.get_or_create(
                        people_entrance=trance, entrance_date=entrance_date,
                        defaults={
                            'project': project
                        })
                    if not created:
                        en_list.project = project
                        en_list.save(update_fields=['project'])
                except IntegrityError:
                    pass
            except PeopleEntrance.DoesNotExist:
                pass

            # 构造orderedDict
            rows = OrderedDict([('id', id), ('people', people.id), ('entrance_type', entrance_type), 
                                ('team', team.id), ('project', project.id), ('entrance_note', entrance_note), 
                                ('end_time', end_time)])
            row_result = self.import_row(
                rows, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', rows))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            else:
                result.totals[row_result.import_type] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleIdleResource(resources.ModelResource):
    """人员闲置情况登记表导入函数"""
    id = fields.Field(column_name='id', attribute='id')
    people = fields.Field(column_name='people', attribute='people', widget=ForeignKeyWidget(People, 'id'))
    note = fields.Field(column_name='note', attribute='note')
    idle_date = fields.Field(column_name='idle_date', attribute='idle_date')
    idle_day = fields.Field(column_name='idle_day', attribute='idle_day')

    class Meta:
        model = PeopleIdle
        fields = ()
        import_id_fields = ('id', 'people', 'note')
    
    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = ['id', '姓名', '备注']
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset) - 2
        for row in dataset.dict:
            error = ''
            id = row['id']
            name = row['姓名']
            try:
                note = row['备注']
            except:
                note = ''
            idle_date = row['闲置月份']
            idle_day = row['闲置天数']
            if id is None or name is None:
                continue
            try:
                name = name.strip()
                note = note.strip()
            except:
                error += '值不能为空'
            try:
                people = People.objects.get(name=name)
            except People.DoesNotExist:
                error += '当前人员{0}不存在！'.format(name)
            try:
                # 防止重复数据产生
                e_idle = PeopleIdle.objects.get(people=people.id)
                if e_idle.id != id:
                    continue
                try:
                    en_list, created = PeopleIdleList.objects.get_or_create(
                        people_idle=e_idle, idle_date=idle_date,
                        defaults={
                            'idle_day': idle_day
                        })
                    if not created:
                        en_list.idle_day = idle_day
                        en_list.save(update_fields=['idle_day'])
                except IntegrityError:
                    pass
            except PeopleIdle.DoesNotExist:
                pass
            # 构造orderedDict
            rows = OrderedDict([('id', id), ('people', people.id), ('note', note)])
            row_result = self.import_row(
                rows, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', rows))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            else:
                result.totals[row_result.import_type] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class ProjectResourceResource(resources.ModelResource):
    """团队项目资源使用情况导入函数"""
    # id, project, order_month, service_people, sum_perple_month, remaining_month, attendance_month
    id = fields.Field(column_name='id', attribute='id')
    project = fields.Field(column_name='project', attribute='project', widget=ForeignKeyWidget(Project, 'id'))
    order_month = fields.Field(column_name='order_month', attribute='order_month')
    service_people = fields.Field(column_name='service_people', attribute='service_people')
    sum_perple_month = fields.Field(column_name='sum_perple_month', attribute='sum_perple_month')
    remaining_month = fields.Field(column_name='remaining_month', attribute='remaining_month')
    attendance_month = fields.Field(column_name='attendance_month', attribute='attendance_month')

    class Meta:
        model = ProjectResource
        fields = ()
        import_id_fields = ('id', 'project', 'order_month', 'service_people', 'sum_perple_month', 
                             'remaining_month', 'attendance_month')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = ['id', '项目',  '订单人月', '服务人员数量', 
                               '累计已投入人月合计', '剩余投入人月', '已考勤人月合计']
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset) 
        for row in dataset.dict:
            error = ''
            id = row['id']
            project_name = row['项目']
            order_month = row['订单人月']
            service_people = row['服务人员数量']
            sum_perple_month = row['累计已投入人月合计']
            remaining_month = row['剩余投入人月']
            attendance_month = row['已考勤人月合计']
            project_name = project_name.strip()
            try:
                project = Project.objects.get(project_name=project_name)
                print(project.id)
            except Project.DoesNotExist:
                error += '项目{0}不存在！'.format(project_name)
            # 构造orderedDict
            rows = OrderedDict([('id', id), ('project', project.id), ('order_month', order_month), 
                                ('service_people', service_people), ('project', project.id), ('sum_perple_month', sum_perple_month), 
                                ('remaining_month', remaining_month), ('attendance_month', attendance_month)])
            row_result = self.import_row(
                rows, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', rows))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            else:
                result.totals[row_result.import_type] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result

