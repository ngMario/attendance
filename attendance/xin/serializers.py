# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/1/14 10:18
# @File    : serializers.py
from __future__ import unicode_literals

from rest_framework import serializers
from .models import Customer


class CustomerPostSerializer(serializers.Serializer):
    name = serializers.CharField(label='姓名', help_text='姓名')
    tel = serializers.CharField(label='手机号', help_text='手机号')
    car_plate = serializers.CharField(label='车牌', help_text='车牌', allow_null=True, allow_blank=True)


class CustomerAddressPostSerializer(serializers.Serializer):
    name = serializers.CharField(label='姓名', help_text='姓名')
    tel = serializers.CharField(label='手机号', help_text='手机号')
    car_plate = serializers.CharField(label='车牌', help_text='车牌', allow_null=True, allow_blank=True)
    address = serializers.CharField(label='地址', help_text='地址')


class CustomerSellCarPostSerializer(serializers.Serializer):
    name = serializers.CharField(label='姓名', help_text='姓名')

    tel = serializers.CharField(label='手机号', help_text='手机号')
    brand = serializers.CharField(label='车辆品牌', help_text='车辆品牌')
    car_models = serializers.CharField(label='车辆型号', help_text='车辆型号， 如保时捷911等')
    car_type = serializers.CharField(label='车辆类型', help_text='车辆类型', allow_null=True, allow_blank=True)
    car_plate = serializers.CharField(label='车牌', help_text='车牌', allow_null=True, allow_blank=True)
    engine_number = serializers.CharField(label='发动机号码后6位', help_text='发动机号码后6位',
                                          allow_null=True, allow_blank=True)
    address = serializers.CharField(label='地址', help_text='地址')
