from django.apps import AppConfig


class SundryConfig(AppConfig):
    name = 'sundry'
    verbose_name = '用户跑分'
