# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/11/20 15:00
# @File    : create_new_user_shell.py
from __future__ import unicode_literals

from datetime import datetime

import requests


def open_data():
    file = open('create_user.txt', 'r', encoding='UTF-8')
    file_data = file.readlines()
    data_list = []
    for row in file_data:
        tmp_list = row.split(',')
        tmp_list[-1] = tmp_list[-1].replace('\n', '')
        data_list.append(tmp_list)
    print(data_list)
    return data_list


if __name__ == '__main__':
    datas = open_data()
    for data in datas:
        name = data[0]
        username = data[1]
        email = data[2]
        num=data[3]
        data = {
            'name': name,
            'username': username,
            'email': email,
            'num': num,
        }
        get_url = 'http://127.0.0.1:8000/api/v1/accounts/create_user/'
        result = requests.post(get_url, data)
        print(data)
