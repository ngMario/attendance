# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/10/30 10:51
# @File    : excel_to_sqlite.py
from __future__ import unicode_literals

import openpyxl
from xpinyin import Pinyin


if __name__ == '__main__':
    p = Pinyin()
    wb = openpyxl.load_workbook('项目人员分组.xlsx')
    ws = wb.active
    ss = ws['C']
    i = 1
    for s in ss:
        name = s.value
        py = p.get_pinyin(name, '')
        ws.cell(row=i,column=4,value=py)
        i += 1
    wb.save('项目人员分组.xlsx')




