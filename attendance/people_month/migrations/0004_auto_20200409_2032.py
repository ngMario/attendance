# Generated by Django 2.2.6 on 2020-04-09 12:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('people_month', '0003_people_responsibility'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inputproject',
            name='start_time',
            field=models.DateField(blank=True, null=True, verbose_name='实际投入项目开始时间'),
        ),
    ]
