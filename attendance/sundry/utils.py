# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/7 15:28
# @File    : utils.py
from __future__ import unicode_literals

import requests


bank_abb = {
    "CCB": "中国建设银行",
    "ABC": "中国农业银行",
    "ICBC": "中国工商银行",
    "BOC": "中国银行",
    "CMBC": "中国民生银行",
    "CMB": "招商银行",
    "CIB": "兴业银行",
    "BCM": "交通银行",
    "CITICIB": "中信银行",
    "PSBC": "中国邮政银行",
}


def get_bank(cardNo):
    url = "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json"
    params = {
        "_input_charset": "utf-8",
        "cardNo": cardNo,
        "cardBinCheck": "true",
    }
    try:
        bank = requests.get(url=url, params=params).json()["bank"]
    except:
        return 1
    if bank in bank_abb:
        return bank_abb[bank]
    else:
        return bank


if __name__ == '__main__':
    print(get_bank("622asfdasdf024100023165040"))
