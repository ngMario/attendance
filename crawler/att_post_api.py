# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/10/30 17:12
# @File    : att_post_api.py
from __future__ import unicode_literals

from datetime import datetime

import requests


def open_data():
    filename = datetime.today().strftime("%Y-%m-%d") + '.txt'
    file = open(filename, 'r')
    file_data = file.readlines()
    data_list = []
    for row in file_data:
        tmp_list = row.split(',')
        tmp_list[-1] = tmp_list[-1].replace('\n', '')
        data_list.append(tmp_list)
    return data_list


if __name__ == '__main__':
    datas = open_data()
    for data in datas:
        attendance_group = data[0]
        attendance_date = data[1]
        in_date = data[2]
        back_date = data[3]
        data = {
            'attendance_group': attendance_group,
            'attendance_date': attendance_date,
            'in_date': in_date,
            'back_date': back_date,
        }
        get_url = 'http://127.0.0.1:8000/api/v1/accounts/attendance/'
        result = requests.post(get_url, data)

