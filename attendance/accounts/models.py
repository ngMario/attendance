﻿# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        '昵称', max_length=30, unique=True, help_text='昵称长度4-20个字符，支持中英文、数字、-、_',
        validators=[
            validators.RegexValidator('^[a-zA-Z0-9-_\u4e00-\u9fa5]+$',
                                      '昵称长度4-20个字符，支持中英文、数字、-、_', 'invalid')
        ])
    email = models.EmailField('邮箱', default=None, unique=True, null=True, blank=True)
    password = models.CharField('密码', blank=True, null=True, max_length=128)
    is_password_set = models.BooleanField('是否设置密码', default=True,
                                          help_text=_('true：设置密码，false：随机密码(手机验证码注册时随机生成)'))

    date_joined = models.DateTimeField('注册时间', default=timezone.now)
    is_staff = models.BooleanField('是否是职员', default=False)
    is_leader = models.BooleanField('是否是组长', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = '系统用户'
        verbose_name_plural = verbose_name


class ProjectGroup(models.Model):
    project_name = models.CharField('项目组名称', max_length=50)
    project_note = models.CharField('工作描述', max_length=2000, null=True, blank=True)

    def __str__(self):
        return '%s' % self.project_name

    class Meta:
        verbose_name = '项目组'
        verbose_name_plural = verbose_name


class ProjectPeople(models.Model):
    project = models.ForeignKey(ProjectGroup, related_name='project_people', on_delete=models.CASCADE,
                                verbose_name='项目组')
    name = models.CharField('姓名', max_length=10, unique=True)
    user = models.ForeignKey(User, related_name='project_people_user', on_delete=models.CASCADE,
                             verbose_name='用户', null=True, blank=True)
    attendance_group = models.CharField('考勤系统用户名', max_length=30, null=True, blank=True)
    position = models.SmallIntegerField('显示的位置', default=0, help_text='值越大越靠前')

    TEAM_TYPE = (
        (0, '安全小组'),
        (1, '数据小组'),
        (2, '采购组'),
    )
    team_type = models.SmallIntegerField('所属小组', choices=TEAM_TYPE, default=0)
    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = '人员表'
        verbose_name_plural = verbose_name


class Attendance(models.Model):
    people = models.ForeignKey(ProjectPeople, related_name='attendance_people', on_delete=models.CASCADE,
                               verbose_name='姓名')
    attendance_date = models.DateField('考勤时间', default=timezone.now)
    reason = models.CharField('具体事由', max_length=500, null=True, blank=True)
    in_date = models.TimeField('签到时间')
    back_date = models.TimeField('签退时间')
    NOTE_TYPE = (
        ('M', '加班'),
        ('F', '考勤')
    )
    note = models.CharField('备注', choices=NOTE_TYPE, max_length=10,
                            default='F')

    def __str__(self):
        return '%s' % self.attendance_date

    class Meta:
        verbose_name = '考勤表'
        verbose_name_plural = verbose_name


class Weekly(models.Model):
    people = models.ForeignKey(ProjectPeople, related_name='weekly_people',
                               on_delete=models.CASCADE, verbose_name='姓名')
    update_time = models.DateField('填表日期', default=timezone.now)

    def __str__(self):
        return '%s' % self.people.name

    class Meta:
        verbose_name = '周报'
        verbose_name_plural = verbose_name


class WeekContent(models.Model):
    week_weekly = models.ForeignKey(Weekly, verbose_name='周报',
                                    related_name='weekly_week_content',
                                    on_delete=models.CASCADE)
    week_content = models.CharField('工作事项名称', max_length=500)
    start_time = models.DateField('开始时间', default=timezone.now)
    end_time = models.DateField('结束时间', default=timezone.now)
    plan = models.IntegerField('计划%', help_text='%', null=True, blank=True, default=100)
    actual = models.IntegerField('实际%', help_text='%', null=True, blank=True, default=100)
    deviation = models.IntegerField('偏差%', help_text='%', null=True, blank=True, default=0)
    achievements = models.CharField('本周工作成果', max_length=1500, default="完成")
    progress_desc = models.CharField('进展说明(填写目前存在的问题以及需要协调解决的事项)', max_length=1500,
                                     help_text='填写内容为目前存在的问题以及需要协调解决的事项', default="完成")

    def __str__(self):
        return '%s' % self.week_content

    class Meta:
        verbose_name = '本周工作内容'
        verbose_name_plural = verbose_name


class NextWeekContent(models.Model):
    net_week_weekly = models.ForeignKey(Weekly, verbose_name='周报',
                                        related_name='weekly_next_week_content',
                                        on_delete=models.CASCADE)
    next_week_content = models.CharField('工作事项名称', max_length=500, null=True, blank=True)
    expect_time = models.DateField('开始时间', default=timezone.now)
    plan_time = models.DateField('计划完成时间', default=timezone.now)
    result = models.CharField('计划输出结果', max_length=500, null=True, blank=True)
    note = models.CharField('说明', max_length=500, null=True, blank=True)

    def __str__(self):
        return '%s' % self.next_week_content

    class Meta:
        verbose_name = '下周计划工作内容'
        verbose_name_plural = verbose_name


class GitPushFile(models.Model):
    # 文件上传
    file_upload = models.FileField(upload_to='file')

    def __str__(self):
        return '%s' % self.file_upload

    class Meta:
        verbose_name = '周报上传'
        verbose_name_plural = verbose_name

