from django.apps import AppConfig


class PeopleMonthConfig(AppConfig):
    name = 'people_month'
    verbose_name = '人月管理系统'