# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/9/27 16:58
# @File    : api_urls.py
from __future__ import unicode_literals

from django.conf.urls import include, url
from .router import router, router_no_slash


urlpatterns = [
    url(r'^accounts/', include('accounts.api_urls')),
    url(r'^xin/', include('xin.api_urls')),
    url(r'^sundry/', include('sundry.api_urls')),
]

urlpatterns += [
    url(r'', include(router.urls)),
    url(r'', include(router_no_slash.urls)),
]
