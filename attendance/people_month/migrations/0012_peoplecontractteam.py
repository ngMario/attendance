# Generated by Django 2.2.6 on 2020-07-01 08:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people_month', '0011_thejobinfo_team'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeopleContractTeam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateField(blank=True, null=True, verbose_name='投入时间')),
                ('end_time', models.DateField(blank=True, null=True, verbose_name='结束时间')),
                ('is_start', models.BooleanField(default=False, verbose_name='人员是否结束投入')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='修改时间')),
                ('people', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='people_contract_team', to='people_month.People', verbose_name='姓名')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='people_contract_team', to='people_month.Team', verbose_name='团队')),
            ],
            options={
                'verbose_name': '团队和人员投入关系表',
                'verbose_name_plural': '团队和人员投入关系表',
            },
        ),
    ]
