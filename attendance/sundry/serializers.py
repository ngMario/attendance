# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/7 15:16
# @File    : serializers.py
from __future__ import unicode_literals

from rest_framework import serializers


class WithdrawalRecordSerializers(serializers.Serializer):
    name = serializers.CharField(label='姓名', help_text='姓名')
    bank_card_no = serializers.CharField(label='银行卡号', help_text='银行卡号')
    amount = serializers.DecimalField(max_digits=12, decimal_places=2, label='提现金额', help_text='提现金额')
