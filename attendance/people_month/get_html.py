# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/2 17:31
# @File    : get_html.py
from __future__ import unicode_literals

from datetime import datetime, timedelta

import people_month.utils as utils


def old_experts_html(obj):
    # 资深专家总天数
    old_experts_total_day = utils.old_experts_total_day(obj)
    # 资深专家人月数
    old_experts_month = round(old_experts_total_day / 22.0, 2)
    # 资深专家合同数
    old_experts_ps = obj.senior_month
    # 差异
    try:
        differences = old_experts_ps - old_experts_month
    except:
        differences = 0
    # 资深专家在职人数
    old_experts = utils.old_experts(obj)
    # 资深专家按现有人数打卡仍需/月
    try:
        need = round(differences / old_experts, 2)
    except:
        need = 0
    # 资深专家预计完成时间
    if obj.senior_month is None:
        senior_month = 0
    else:
        senior_month = obj.senior_month
    if old_experts == 0:
        old_date = senior_month * 22
    else:
        old_date = (senior_month / old_experts) * 22
    if obj.start_time is None:
        complete = ''
    else:
        complete = datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    return old_experts_total_day, old_experts_month, old_experts_ps, differences, old_experts, need, complete


def experts_html(obj):
    """专家"""
    # 专家总天数
    experts_total_day = utils.experts_total_day(obj)
    # 专家人月数
    experts_month = round(experts_total_day / 22.0, 2)
    # 专家合同数
    experts_ps = obj.experts_month
    # 差异
    try:
        differences = experts_ps - experts_month
    except:
        differences = 0
    # 专家在职人数
    experts = utils.experts(obj)
    # 专家按现有人数打卡仍需/月
    try:
        need = round(differences / experts, 2)
    except:
        need = 0
    # 专家预计完成时间
    if obj.experts_month is None:
        experts_month = 0
    else:
        experts_month = obj.experts_month
    if experts == 0:
        old_date = experts_month * 22
    else:
        old_date = (experts_month / experts) * 22
    if obj.start_time is None:
        complete = ''
    else:
        complete = datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    return experts_total_day, experts_month, experts_ps, differences, experts, need, complete


def senior_html(obj):
    """高级"""
    # 高级总天数
    senior_total_day = utils.senior_total_day(obj)
    # 高级人月数
    senior_month = round(senior_total_day / 22.0, 2)
    # 高级合同数
    senior_ps = obj.high_month
    # 差异
    try:
        differences = senior_ps - senior_month
    except:
        differences = 0
    # 高级在职人数
    senior = utils.senior(obj)
    # 高级按现有人数打卡仍需/月
    try:
        need = round(differences / senior, 2)
    except:
        need = 0
    # 高级预计完成时间
    if obj.high_month is None:
        high_month = 0
    else:
        high_month = obj.high_month
    if senior == 0:
        old_date = high_month * 22
    else:
        old_date = (high_month / senior) * 22
    if obj.start_time is None:
        senior_complete = ''
    else:
        senior_complete = datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    return senior_total_day, senior_month, senior_ps, differences, senior, need, senior_complete


def intermediate_html(obj):
    """中级"""
    # 中级总天数
    intermediate_total_day = utils.intermediate_total_day(obj)
    # 中级人月数
    intermediate_month = round(intermediate_total_day / 22.0, 2)
    # 中级合同数
    intermediate_ps = obj.intermediate_month
    # 差异
    try:
        intermediate_differences = intermediate_ps - intermediate_month
    except:
        intermediate_differences = 0
    # 中级在职人数
    intermediate = utils.intermediate(obj)
    # 中级按现有人数打卡仍需/月
    try:
        intermediate_need = round(intermediate_differences / intermediate, 2)
    except:
        intermediate_need = 0
    # 中级预计完成时间
    if obj.intermediate_month is None:
        intermediate_month = 0
    else:
        intermediate_month = obj.intermediate_month
    if intermediate == 0:
        old_date = intermediate_month * 22
    else:
        old_date = (intermediate_month / intermediate) * 22
    if obj.start_time is None:
        intermediate_complete = ''
    else:
        intermediate_complete = datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    return intermediate_total_day, intermediate_month, intermediate_ps, intermediate_differences, intermediate,\
           intermediate_need, intermediate_complete


def primary_html(obj):
    """初级"""
    # 初级总天数
    primary_total_day = utils.primary_total_day(obj)
    # 初级人月数
    primary_month = round(primary_total_day / 22.0, 2)
    # 初级合同数
    primary_ps = obj.primary_month
    # 差异
    try:
        primary_differences = primary_ps - primary_month
    except:
        primary_differences = 0
    # 初级在职人数
    primary = utils.primary(obj)
    # 初级按现有人数打卡仍需/月
    try:
        primary_need = round(primary_differences / primary, 2)
    except:
        primary_need = 0
    # 初级预计完成时间
    if obj.primary_month is None:
        primary_month = 0
    else:
        primary_month = obj.primary_month
    if primary == 0:
        old_date = primary_month * 22
    else:
        old_date = (primary_month / primary) * 22
    if obj.start_time is None:
        primary_complete = ''
    else:
        primary_complete = datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    return primary_total_day, primary_month, primary_ps, primary_differences, primary,\
           primary_need, primary_complete
