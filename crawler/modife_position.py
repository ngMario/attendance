# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/12/2 10:46
# @File    : modife_position.py
from __future__ import unicode_literals

import requests


def open_data():
    file = open('position.txt', 'r', encoding='UTF-8')
    file_data = file.readlines()
    data_list = []
    for row in file_data:
        tmp_list = row.split(',')
        tmp_list[-1] = tmp_list[-1].replace('\n', '')
        data_list.append(tmp_list)
    return data_list


if __name__ == '__main__':
    datas = open_data()
    for data in datas:
        position = data[0]
        name = data[1].strip()
        print(position, name)
        new_data = {
            'name': name,
            'position': position
        }
        get_url = 'http://127.0.0.1:8000/api/v1/position/modife_position?name={0}&position={1}'.format(name, position)
        print(get_url)
        result = requests.get(get_url)
