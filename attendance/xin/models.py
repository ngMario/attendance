# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class Customer(models.Model):
    name = models.CharField('联系人', max_length=20, help_text='车主联系人')
    tel = models.CharField('手机号', max_length=20, help_text='车主手机号')
    car_type = models.CharField('车辆类型', max_length=100, help_text='suv, 轿车等',
                                null=True, blank=True)
    ORDER_TYPE = (
        (0, '客户自行上门车检'),
        (1, '公司人员去客户那边取车'),
        (2, '卖车')
    )
    order_type = models.SmallIntegerField('车检方式', choices=ORDER_TYPE, default=0)
    car_plate = models.CharField('车牌', max_length=20, help_text='车牌号码')
    brand = models.CharField('车辆品牌', max_length=100, help_text='车辆品牌', null=True, blank=True)
    car_models = models.CharField('车辆型号', max_length=100, help_text='车辆型号， 如保时捷911等',
                                  null=True, blank=True)
    engine_number = models.CharField('发动机号码后6位', max_length=20, null=True, blank=True)
    order_date = models.DateTimeField('预约日期', default=timezone.now)
    get_out_date = models.DateTimeField('取车日期', null=True, blank=True)
    address = models.CharField('取车地址', max_length=100, null=True,
                               blank=True, help_text='去客户那取车的地址')

    def __unicode__(self):
        return '%s' % (self.name, )

    class Meta:
        verbose_name = '预约客户信息'
        verbose_name_plural = verbose_name