# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/4/3 15:36
# @File    : admin.py
from __future__ import unicode_literals

from datetime import datetime, date
from import_export import resources
from datetime import datetime, timedelta
from chinese_calendar import is_holiday

from .models import *
from django.db.models import Sum


def num_jobs_day(job_info):
    try:
        s = job_info[0]['num_jobs_day']
    except:
        s = 0
    return s


def qualification(schooling, graduation_time):
    w_year = date.today() - graduation_time
    w_year = round((w_year.days / 365), 2)
    if schooling == 0:
        if w_year < 6:
            qualification = '初级'
        elif w_year >=6 and w_year < 8:
            qualification = '中级'
        else:
            qualification = '高级'
    elif schooling == 1:
        if w_year < 4:
            qualification = '初级'
        elif w_year >=4 and w_year < 6:
            qualification = '中级'
        elif w_year >=6 and w_year < 8:
            qualification = '高级'
        elif w_year >=8 and w_year < 10:
            qualification = '专家'
        else:
            qualification = '资深专家'
    elif schooling == 2:
        if w_year < 4:
            qualification = '中级'
        elif w_year >=4 and w_year < 6:
            qualification = '高级'
        elif w_year >=6 and w_year < 8:
            qualification = '专家'
        else:
            qualification = '资深专家'
    elif schooling == 3:
        if w_year < 4:
            qualification = '高级'
        elif w_year >=4 and w_year < 6:
            qualification = '专家'
        else:
            qualification = '资深专家'
    elif schooling == 4:
        if w_year < 4:
            qualification = '专家'
        else:
            qualification = '资深专家'
    else:
        if w_year < 4:
            qualification = '初级'
        else:
            qualification = '中级'
    return qualification


def work_year(graduation_time):
    w_year = date.today() - graduation_time
    w_year = round((w_year.days / 365), 2)
    return int(w_year)


def server_people(obj):
    """项目人数"""
    project = PeopleContractProject.objects.filter(contract_project=obj.id, is_start=False).count()
    return project


def sum_input_month(obj):
    """已投入人月"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_ids, info_project_name=obj.id)
    s = num_jobs_day(job_info)
    return round(s / 22.0, 2)


def remaining(obj):
    """
    剩余投入人月
    """
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_ids, info_project_name=obj.id)
    return round(obj.contract_month - round(num_jobs_day(job_info) / 22.0, 2), 2)


def input_month(obj):
    """已考勤人月"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_ids, info_project_name=obj.id)
    return round(num_jobs_day(job_info) / 22.0, 2)


def old_experts(obj):
    """资深专家人数"""
    projects = projects_ids_old(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    counts = 0
    for people in peoples:
        if people.admission_qualification == 4:
            counts += 1
    return counts


def experts(obj):
    """专家人数"""
    projects = projects_ids_old(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    counts = 0
    for people in peoples:
        if people.admission_qualification == 3:
            counts += 1
    return counts


def senior(obj):
    """高级人数"""
    projects = projects_ids_old(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    counts = 0
    for people in peoples:
        if people.admission_qualification == 2:
            counts += 1
    return counts


def intermediate(obj):
    """中级人数"""
    projects = projects_ids_old(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    counts = 0
    for people in peoples:
        if people.admission_qualification == 1:
            counts += 1
    return counts


def primary(obj):
    """初级人数"""
    projects = projects_ids_old(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    counts = 0
    for people in peoples:
        if people.admission_qualification == 0:
            counts += 1
    return counts


def total_day(obj):
    """总天数"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_ids, info_project_name=obj.id)
    return num_jobs_day(job_info)


def primary_total_day(obj):
    """初级工程师总天数"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    people_counts = []
    for people in peoples:
        if people.admission_qualification == 0:
            people_counts.append(people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_counts, info_project_name=obj.id)
    try:
        total_day = job_info[0]['num_jobs_day']
    except:
        total_day = 0
    return total_day


def intermediate_total_day(obj):
    """中级工程师总天数"""
    projects = PeopleContractProject.objects.filter(contract_project=obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    people_counts = []
    for people in peoples:
        if people.admission_qualification == 1:
            people_counts.append(people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_counts, info_project_name=obj.id)
    try:
        total_day = job_info[0]['num_jobs_day']
    except:
        total_day = 0
    return total_day


def senior_total_day(obj):
    """高级工程师总天数"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    people_counts = []
    for people in peoples:
        if people.admission_qualification == 2:
            people_counts.append(people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_counts, info_project_name=obj.id)
    try:
        total_day = job_info[0]['num_jobs_day']
    except:
        total_day = 0
    return total_day


def experts_total_day(obj):
    """专家总天数"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    people_counts = []
    for people in peoples:
        if people.admission_qualification == 3:
            people_counts.append(people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_counts, info_project_name=obj.id)
    try:
        total_day = job_info[0]['num_jobs_day']
    except:
        total_day = 0
    return total_day


def old_experts_total_day(obj):
    """资深专家总天数"""
    projects = projects_ids(obj.id)
    people_ids = []
    for project in projects:
        people_ids.append(project.people.id)
    peoples = People.objects.filter(id__in=people_ids)
    people_counts = []
    for people in peoples:
        if people.admission_qualification == 4:
            people_counts.append(people.id)
    job_info = TheJobInfo.objects.values('jobs_day').annotate(
        num_jobs_day=Sum('jobs_day')).filter(people_id__in=people_counts, info_project_name=obj.id)
    try:
        total_day = job_info[0]['num_jobs_day']
    except:
        total_day = 0
    return total_day


def projects_ids(obj_id):
    return PeopleContractProject.objects.filter(contract_project=obj_id)


def projects_ids_old(obj_id):
    return PeopleContractProject.objects.filter(contract_project=obj_id, is_start=False)


def tradedays(start, end):
    '''
    计算两个日期间的工作日
    start:开始时间
    end:结束时间
    '''
    # 字符串格式日期的处理
    if type(start) == str:
        start = datetime.strptime(start, '%Y-%m-%d').date()
    if type(end) == str:
        end = datetime.strptime(end, '%Y-%m-%d').date()
    # 开始日期大，颠倒开始日期和结束日期
    if start > end:
        start, end = end, start

    counts = 0
    while True:
        if start > end:
            break
        if is_holiday(start) or start.weekday() == 5 or start.weekday() == 6:
            start += timedelta(days=1)
            continue
        counts += 1
        start += timedelta(days=1)
    return counts


def return_total_s(obj):
    s = ""
    # 计算从入职到第一次投入的时间之间的闲置天数
    try:
        contract = PeopleContractProject.objects.values('start_time').filter(
            people=obj).order_by('start_time')
        count = 0
        start_time = ""
        for con in contract:
            if count >= 1:
                continue
            start_time = con['start_time']
            count += 1
        if obj.entry_time > start_time:
            contract_total = 0
        else:
            contract_total = tradedays(
                date.strftime(obj.entry_time, "%Y-%m-%d"), date.strftime(start_time, "%Y-%m-%d"))
            s += date.strftime(obj.entry_time, "%Y%m%d") + "入职厦开，转入分公司闲置," \
                 + date.strftime(start_time, "%Y%m%d") + "结束闲置."
    except Exception:
        contract_total = tradedays(
            date.strftime(obj.entry_time, "%Y-%m-%d"), date.strftime(date.today(), "%Y-%m-%d"))
    if s == "":
        s += date.strftime(obj.entry_time, "%Y%m%d") + "入职厦开，转入分公司闲置."
    # 上一个计算投入项目到下一个投入项目之间闲置的天数
    try:
        start_contract = PeopleContractProject.objects.filter(
            people=obj).order_by('start_time')
        a = 0
        sta_time = ""
        en_time = ""
        counts = 0
        if len(start_contract) > 1:
            for start in start_contract:
                if start.end_time is not None:
                    s += date.strftime(start.start_time, "%Y-%m-%d") + "投入《" + start.contract_project.project_name \
                         + "》项目， " + date.strftime(start.end_time, "%Y-%m-%d") + "结束投入。"
                else:
                    s += date.strftime(start.start_time, "%Y-%m-%d") + "投入《" + start.contract_project.project_name \
                         + "》项目， "
                if a == 0:
                    sta_time = start.end_time
                else:
                    en_time = start.start_time
                if a != 0:
                    counts = tradedays(date.strftime(sta_time, "%Y-%m-%d"),
                                       date.strftime(en_time, "%Y-%m-%d"))
                a += 1
    except Exception:
        counts = 0
    total = contract_total + counts

    return total, s
