# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/1/14 10:21
# @File    : api.py
from __future__ import unicode_literals

from datetime import datetime

from rest_framework import viewsets
from rest_framework.decorators import action
from django.conf import settings

from common.serializers import NoneSerializer
from common.api import APIResponse
from .serializers import CustomerPostSerializer, CustomerAddressPostSerializer, CustomerSellCarPostSerializer
from .models import Customer
# from common.send_message import send_message


class AppointmentViewSet(viewsets.GenericViewSet):
    serializer_class = NoneSerializer

    def get_serializer_class(self):
        if 'create' == self.action:
            return CustomerPostSerializer
        elif self.action == 'sell_car':
            return CustomerSellCarPostSerializer
        else:
            return CustomerAddressPostSerializer

    def create(self, request, *args, **kwargs):
        '''
        接口描述: 客户自提车提交预约信息
        ------------------------------

        成功返回示例:
        -------------
            {
              "status": True,
            }

        错误返回:
        ---------

            =======================         ============================================================
                  error_key                             response
            =======================         ============================================================
                params_error                        参数错误
            =======================         ============================================================
        '''
        sz = CustomerPostSerializer(data=request.data)
        if not sz.is_valid():
            return APIResponse(errors=sz.errors)
        name = sz.validated_data.get('name')
        tel = sz.validated_data.get('tel')
        car_plate = sz.validated_data.get('car_plate')
        Customer.objects.create(
            name=name, tel=tel, order_type=0, car_plate=car_plate)
        od_date = datetime.strftime(datetime.today(), "%m-%d")
        title = '自行上门检测'
        sms_message = {"name": name,
                       "date": od_date,
                       "title": title,
                       "phone": tel}
        # send_message(settings.SMS_MOBILE, sms_message)
        return APIResponse({'status': True})

    @action(detail=False, methods=['post'])
    def address_create(self, request, *args, **kwargs):
        '''
        接口描述: 公司人员上门提车
        ------------------------------

        成功返回示例:
        -------------
            {
              "status": True,
            }

        错误返回:
        ---------

            =======================         ============================================================
                  error_key                             response
            =======================         ============================================================
                params_error                        参数错误
            =======================         ============================================================
        '''
        sz = CustomerAddressPostSerializer(data=request.data)
        if not sz.is_valid():
            return APIResponse(errors=sz.errors)
        name = sz.validated_data.get('name')
        tel = sz.validated_data.get('tel')
        car_plate = sz.validated_data.get('car_plate')
        address = sz.validated_data.get('address')
        Customer.objects.create(
            name=name, tel=tel, order_type=1,
            car_plate=car_plate, address=address
        )
        od_date = datetime.strftime(datetime.today(), "%m-%d")
        title = '联系客户上门取车,地址:' + address + ' '
        sms_message = {"name": name,
                       "date": od_date,
                       "title": title,
                       "phone": tel}
        # send_message(settings.SMS_MOBILE, sms_message)
        return APIResponse({'status': True})

    @action(detail=False, methods=['post'])
    def sell_car(self, request, *args, **kwargs):
        '''
        接口描述: 卖车信息提交
        ------------------------------

        成功返回示例:
        -------------
            {
              "status": True,
            }

        错误返回:
        ---------

            =======================         ============================================================
                  error_key                             response
            =======================         ============================================================
                params_error                        参数错误
            =======================         ============================================================
        '''
        sz = CustomerSellCarPostSerializer(data=request.data)
        if not sz.is_valid():
            return APIResponse(errors=sz.errors)
        name = sz.validated_data.get('name')
        tel = sz.validated_data.get('tel')
        brand = sz.validated_data.get('brand')
        car_models = sz.validated_data.get('car_models')
        car_type = sz.validated_data.get('car_type')
        car_plate = sz.validated_data.get('car_plate')
        engine_number = sz.validated_data.get('engine_number')
        address = sz.validated_data.get('address')
        Customer.objects.create(
            name=name, tel=tel, car_type=car_type, order_type=2, brand=brand, car_models=car_models,
            car_plate=car_plate, engine_number=engine_number, address=address
        )
        od_date = datetime.strftime(datetime.today(), "%m-%d")
        title = car_type + '卖车信息'+ brand + car_models
        sms_message = {"name": name,
                       "date": od_date,
                       "title": title,
                       "phone": tel}
        # send_message(settings.SMS_MOBILE, sms_message)
        return APIResponse({'status': True})
