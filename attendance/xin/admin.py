# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Customer


class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'tel', 'car_type', 'car_plate',
                    'engine_number', 'order_date', 'get_out_date', 'order_type',
                    'address', 'brand', 'car_models')

    ordering = ['-id']
    save_on_top = True
    list_per_page = 50


admin.site.register(Customer, CustomerAdmin)