# Generated by Django 2.2.6 on 2020-04-09 12:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('people_month', '0004_auto_20200409_2032'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeopleContractProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateField(blank=True, null=True, verbose_name='投入时间')),
                ('end_time', models.DateField(blank=True, null=True, verbose_name='结束时间')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='修改时间')),
                ('contract_project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='people_contract_project', to='people_month.ContractProject', verbose_name='合同项目')),
                ('people', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='people_contract', to='people_month.People', verbose_name='姓名')),
            ],
            options={
                'verbose_name': '合同项目人员投入关系表',
                'verbose_name_plural': '合同项目人员投入关系表',
            },
        ),
    ]
