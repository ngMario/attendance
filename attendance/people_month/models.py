# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


class Company(models.Model):
    name = models.CharField('供应商公司名称', max_length=50)
    note = models.CharField('描述', max_length=2000, null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = '供应商公司'
        verbose_name_plural = verbose_name


class Team(models.Model):
    team_name = models.CharField('团队名称', max_length=50)
    team_note = models.CharField('团队描述', max_length=2000, null=True, blank=True)
    TEAM_TYPE = (
        (0, '未开始'),
        (1, '已开始'),
        (2, '结束'),
        (3, '未知'),
    )
    team_head = models.CharField('团队负责人', max_length=50, null=True, blank=True)
    team_type = models.SmallIntegerField('团队状态', choices=TEAM_TYPE, default=0)
    start_time = models.DateField('开始时间', null=True, blank=True)
    end_time = models.DateField('结束时间', null=True, blank=True)
    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.team_name

    class Meta:
        verbose_name = '团队'
        verbose_name_plural = verbose_name


class ContractProject(models.Model):
    team = models.ForeignKey(Team, related_name='contract_project_team', on_delete=models.CASCADE,
                             verbose_name='团队')
    project_name = models.CharField('合同项目名称', max_length=50)
    project_note = models.CharField('合同项目描述', max_length=2000, null=True, blank=True)
    PROJECT_TYPE = (
        (0, '未开始'),
        (1, '已开始'),
        (2, '结束'),
        (3, '未知'),
    )
    project_type = models.SmallIntegerField('合同项目状态', choices=PROJECT_TYPE, default=0)
    bank_head = models.CharField('行方负责人', max_length=10, null=True, blank=True)
    company_head = models.CharField('公司负责人', max_length=10, null=True, blank=True)
    cycle = models.FloatField('合同周期', null=True, blank=True)
    notice_time = models.DateField('项目入场通知时间', null=True, blank=True)
    start_time = models.DateField('项目入场时间', null=True, blank=True)
    contract_month = models.FloatField('合同总人月数', default=0, null=True, blank=True)

    senior_month = models.FloatField('资深专家合同人月数', null=True, blank=True)
    experts_month = models.FloatField('专家合同人月数', null=True, blank=True)
    high_month = models.FloatField('高级工程师合同人月数', null=True, blank=True)
    intermediate_month = models.FloatField('中级工程师合同人月数', null=True, blank=True)
    primary_month = models.FloatField('初级工程师合同人月数', null=True, blank=True)
    end_time = models.DateField('项目结束时间', null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.project_name

    class Meta:
        verbose_name = '合同项目'
        verbose_name_plural = verbose_name


class ContractProjectBase(ContractProject):
    class Meta:
        proxy = True
        verbose_name = '团队资源使用情况'
        verbose_name_plural = verbose_name


class ContractProjectBaseInfo(ContractProject):
    class Meta:
        proxy = True
        verbose_name = '项目人月基本情况'
        verbose_name_plural = verbose_name


class ContractProjecStatistical(ContractProject):
    class Meta:
        proxy = True
        verbose_name = '项目基本情况报表'
        verbose_name_plural = verbose_name


class InputProject(models.Model):
    team = models.ForeignKey(Team, related_name='input_project_team', on_delete=models.CASCADE,
                             verbose_name='团队')
    project_name = models.CharField('实际投入项目名称', max_length=50)
    project_note = models.CharField('实际投入项目描述', max_length=2000, null=True, blank=True)
    PROJECT_TYPE = (
        (0, '未开始'),
        (1, '已开始'),
        (2, '结束'),
        (3, '未知'),
    )
    project_type = models.SmallIntegerField('实际投入项目状态', choices=PROJECT_TYPE, default=0)
    start_time = models.DateField('实际投入项目开始时间', null=True, blank=True)
    end_time = models.DateField('实际投入项目结束时间', null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.project_name

    class Meta:
        verbose_name = '实际投入项目'
        verbose_name_plural = verbose_name


class People(models.Model):
    name = models.CharField('姓名', max_length=50)
    company_name = models.ForeignKey(Company, related_name='company_peopke', on_delete=models.CASCADE,
                                     verbose_name='供应商公司', null=True, blank=True)
    mobile = models.CharField('联系方式', max_length=15, null=True, blank=True)
    email = models.EmailField('邮箱', default=None, unique=True, null=True, blank=True)
    entry_time = models.DateField('入职时间', default=timezone.now)

    graduation_time = models.DateField('毕业时间', default=timezone.now)
    direction = models.CharField('专业方向', max_length=200, null=True, blank=True)
    SCHOOLING = (
        (0, '大专'),
        (1, '本科'),
        (2, '硕士'),
        (3, '博士'),
        (4, '博士后'),
        (5, '其他'),
    )
    schooling = models.SmallIntegerField('学历', choices=SCHOOLING, default=0)

    ADMISSION = (
        (0, '初级工程师'),
        (1, '中级工程师'),
        (2, '高级工程师'),
        (3, '专家'),
        (4, '资深专家'),
        (5, '其他'),
    )
    admission_qualification = models.SmallIntegerField('进场级别', choices=ADMISSION, default=0)

    STATE = (
        (0, '在职'),
        (1, '离职'),
        (2, '休假'),
    )
    state = models.SmallIntegerField('在职状态', choices=STATE, default=0)
    resign_date = models.DateField('离职时间', null=True, blank=True)
    responsibility = models.CharField('工作职责', max_length=200, null=True, blank=True)
    note = models.CharField('备注', max_length=2000, null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = '公司人员表'
        verbose_name_plural = verbose_name


class IdlePeople(People):
    class Meta:
        proxy = True
        verbose_name = '人员闲置情况表'
        verbose_name_plural = verbose_name


class TheJobInfo(models.Model):
    people = models.ForeignKey(People, related_name='info_people', on_delete=models.CASCADE,
                               verbose_name='姓名')
    jobs_day = models.FloatField('在岗天数', default=5, null=True, blank=True)
    info_project_name = models.ForeignKey(ContractProject, related_name='the_job_info_project',
                                          on_delete=models.CASCADE, verbose_name='合同项目',
                                          null=True, blank=True)
    team = models.ForeignKey(Team, related_name='the_job_info_team', on_delete=models.CASCADE,
                             verbose_name='团队', null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.jobs_day

    class Meta:
        verbose_name = '在岗天数（原始数据）'
        verbose_name_plural = verbose_name


class PeopleContractProject(models.Model):
    people = models.ForeignKey(People, related_name='people_contract', on_delete=models.CASCADE,
                               verbose_name='姓名')
    contract_project = models.ForeignKey(ContractProject, related_name='people_contract_project', 
                                         on_delete=models.CASCADE, verbose_name='合同项目')
    start_time = models.DateField('投入时间', null=True, blank=True)
    end_time = models.DateField('结束时间', null=True, blank=True)
    is_start = models.BooleanField('人员是否结束投入', default=False)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    class Meta:
        verbose_name = '合同项目人员投入关系表'
        verbose_name_plural = verbose_name


class PeopleContractTeam(models.Model):
    people = models.ForeignKey(People, related_name='people_contract_team', on_delete=models.CASCADE,
                               verbose_name='姓名')
    team = models.ForeignKey(Team, related_name='people_contract_team',
                             on_delete=models.CASCADE, verbose_name='团队')
    start_time = models.DateField('投入时间', null=True, blank=True)
    end_time = models.DateField('结束时间', null=True, blank=True)
    is_start = models.BooleanField('人员是否结束投入', default=False)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    class Meta:
        verbose_name = '团队和人员投入关系表'
        verbose_name_plural = verbose_name
