# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/7/7 15:14
# @File    : api.py
from __future__ import unicode_literals

from rest_framework import viewsets
from rest_framework.decorators import action
from django.db import transaction

from common.api import APIResponse
from .serializers import *
from .utils import *
from .models import *


class WithdrawalRecordViewSet(viewsets.GenericViewSet):
    serializer_class = WithdrawalRecordSerializers

    @action(methods=['post'], detail=False)
    def record(self, request, *args, **kwargs):
        """
        跑分 http://127.0.0.1:8000/api/v1/record/record/
        http://2.wuzhaohui.top:8000/api/v1/record/record/
        """
        sz = WithdrawalRecordSerializers(data=request.data)
        if not sz.is_valid():
            return APIResponse(errors=sz.errors)
        name = sz.validated_data.get('name')
        bank_card_no = sz.validated_data.get('bank_card_no')
        amount = sz.validated_data.get('amount')

        bank_name = get_bank(bank_card_no)
        if bank_name == 1:
            return APIResponse(errors={'date_invalid': ['银行卡输入有误！']})

        with transaction.atomic():
            try:
                accounts = Accounts.objects.get(name=name)
            except Exception:
                return APIResponse(errors={'accounts_invalid': ['用户不存在！']})

            if accounts.status != 1:
                return APIResponse(errors={'accounts_invalid': ['用户未实名认证！']})

            record = WithdrawalRecord(account=accounts, bank_card_no=bank_card_no, bank_name=bank_name,
                                      amount=amount, status=0)
            record.save()

        return APIResponse({'status': True})
