# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/3/27 15:36
# @File    : project_management_admin_info.py
from __future__ import unicode_literals

from django.contrib import admin
from django.db import transaction
from django.http import HttpResponse
from django.conf import settings
from .models import *
from openpyxl import Workbook , load_workbook
from dateutil import parser
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
from xpinyin import Pinyin
from collections import OrderedDict
from import_export.results import Result, RowResult
from import_export import resources, fields
from import_export.admin import ImportMixin, ExportMixin, ImportExportMixin
from import_export.widgets import ManyToManyWidget, ForeignKeyWidget


class TeamResource(resources.ModelResource):

    team_name = fields.Field(column_name='team_name', attribute='team_name')
    team_note = fields.Field(column_name='team_note', attribute='team_note')
    team_head = fields.Field(column_name='team_head',attribute='team_head')
    team_type = fields.Field(column_name='team_type', attribute='team_type')
    start_time = fields.Field(column_name='start_time', attribute='start_time')
    end_time = fields.Field(column_name='end_time', attribute='end_time')

    class Meta:
        model = Team
        fields = ()
        import_id_fields = ('team_name', 'team_note', 'team_head','team_type','start_time','end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        p = Pinyin()
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            #id=row['id']
            team_name = row['team_name'].strip()
            team_note = row['team_note']
            team_head = row['team_head']
            team_type= row['team_type']
            start_time=row['start_time']
            end_time= row['end_time']
            if id is None or team_name is None or team_note is None or team_head is None or team_type is None or start_time is None or end_time is None:
                continue
            #try:
                #project_name = Team.objects.get(id=int(id))
            #except Team.DoesNotExist:
                #error += '项目组{0}不存在！'.format(str(id))
            try:
                team = Team.objects.get(team_name=team_name)
                #team.project_id=int(id)
                team.team_name = team_name
                team.team_note = team_note
                team.team_head = team_head
                team.team_type = team_type
                team.start_time=start_time
                team.end_time=end_time
                team.save()
            except Team.DoesNotExist:
                Team.objects.create(team_name=team_name,
                                    team_note=team_note,
                                    team_head=team_head,
                                    team_type=team_type,
                                    start_time=start_time,
                                    end_time=end_time)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class TeamAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('team_name', 'team_note', 'team_head', 'team_type', 'start_time', 'end_time')
    search_fields = ('team_name',)
    ordering = ['-team_name']
    save_on_top = True
    list_per_page = 50
    resource_class = TeamResource

class Projectsource(resources.ModelResource):
    team = fields.Field(column_name='team', attribute='team')
    project_name = fields.Field(column_name='project_name', attribute='project_name')
    project_note = fields.Field(column_name='project_note',attribute='project_note')
    project_type = fields.Field(column_name='project_type', attribute='project_type')
    bank_head = fields.Field(column_name='bank_head', attribute='bank_head')
    company_head = fields.Field(column_name='company_head', attribute='company_head')
    start_time = fields.Field(column_name='start_time', attribute='start_time')
    end_time = fields.Field(column_name='end_time', attribute='end_time')

    class Meta:
        model = Project
        fields = ()
        import_id_fields = ('team', 'project_name', 'project_note','project_type','bank_head','company_head','start_time','end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            #id=row['id']
            team = row['team']
            project_name = row['project_name']
            project_note = row['project_note']
            project_type= row['project_type']
            bank_head=row['bank_head']
            company_head= row['company_head']
            start_time = row['start_time']
            end_time = row['end_time']
            if team is None or project_name is None or project_note is None or project_type is None or bank_head is None or company_head is None or start_time is None or end_time is None:
                continue
            #try:
                #project_name = Team.objects.get(id=int(id))
            #except Team.DoesNotExist:
                #error += '项目组{0}不存在！'.format(str(id))
            try:
                project = Project.objects.get(team=team)
                #team.project_id=int(id)
                project.team = team
                project.project_name = project_name
                project.project_note = project_note
                project.project_type = project_type
                project.bank_head = bank_head
                project.company_head=company_head
                project.start_time=start_time
                project.end_time=end_time
                project.save()
            except Project.DoesNotExist:
                Project.objects.create(team=team,
                                       project_name=project_name,
                                       project_note=project_note,
                                       project_type=project_type,
                                       bank_head=bank_head,
                                       company_head=company_head,
                                       start_time=start_time,
                                       end_time=end_time)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class ProjectAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('team', 'project_name', 'project_note', 'project_type', 'bank_head', 'company_head', 'start_time',
                    'end_time')
    search_fields = ('team', 'project_name')
    ordering = ['-team']
    save_on_top = True
    list_per_page = 50
    resource_class = Projectsource


class Project_Resource(resources.ModelResource):
    project = fields.Field(column_name='project', attribute='project')
    order_month = fields.Field(column_name='order_month', attribute='order_month')
    service_people = fields.Field(column_name='service_people',attribute='service_people')
    sum_perple_month = fields.Field(column_name='sum_perple_month', attribute='sum_perple_month')
    remaining_month = fields.Field(column_name='remaining_month', attribute='remaining_month')
    attendance_month = fields.Field(column_name='attendance_month', attribute='attendance_month')

    class Meta:
        model = ProjectResource
        fields = ()
        import_id_fields = ('project', 'order_month', 'service_people','sum_perple_month','remaining_month','attendance_month')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            #id=row['id']
            project = row['project']
            order_month = row['order_month']
            service_people = row['service_people']
            sum_perple_month= row['sum_perple_month']
            remaining_month=row['remaining_month']
            attendance_month= row['attendance_month']

            if project is None or order_month is None or service_people is None or sum_perple_month is None or remaining_month is None or attendance_month is None :
                continue
            #try:
                #project_name = Team.objects.get(id=int(id))
            #except Team.DoesNotExist:
                #error += '项目组{0}不存在！'.format(str(id))
            try:
                project_resource = ProjectResource.objects.get(project=project)
                #team.project_id=int(id)
                project_resource.project = project
                project_resource.order_month = order_month
                project_resource.service_people = service_people
                project_resource.sum_perple_month = sum_perple_month
                project_resource.remaining_month = remaining_month
                project_resource.attendance_month=attendance_month
                project.save()
            except ProjectResource.DoesNotExist:
                ProjectResource.objects.create(project=project,
                                       order_month=order_month,
                                       service_people=service_people,
                                       sum_perple_month=sum_perple_month,
                                       remaining_month=remaining_month,
                                       attendance_month=attendance_month)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class ProjectResourceAdmin(ImportMixin, admin.ModelAdmin):
    actions = ['export_as_excel']
    list_display = ('project', 'order_month', 'service_people', 'sum_perple_month', 'remaining_month',
                    'attendance_month')
    search_fields = ('project',)
    ordering = ['project']
    save_on_top = True
    list_per_page = 50
    resource_class = Project_Resource

    def remaining_month(self,obj):
        return round(parser.parse(str(obj.order_month))-parser.parse(str(obj.attendance_month)).month,1)

    def attendance_month(self,obj):
        remaining_month=self.remaining_month()
        headers=['2019年内部认证（UASS&SSMC）维护服务', '2019年NISA系统运维','援建安康智慧治理项目用户统一身份认证开发服务',
                '企业债务风险监测系统（一期）项目安全认证组件开发服务','同业金融财务公司项目云安全组件开发服务','社保基金理事会统一前端整合门户建设开发服务',
                '灾备建设-多活及灾备应用改造项目安全组件开发服务','基础设施安全系统_网络版非涉密计算机保密检查工具集成开发服务',
                '用户认证_指纹认证优化改造项目开发服务']
        result=[]
        for header in headers:
            sum_days=0
            project_filter=PeopleJobs.objects.filter(project=header)
            for days in project_filter.jobs_day:
                sum_days=sum_days+days
            ave_days=round(sum_days/22,1)
            result.append(ave_days)

        if remaining_month>0:
            if obj.project==headers[0]:
                obj.attendance_month=result[0]
            elif obj.project==headers[1]:
                obj.attendance_month=result[1]
            elif obj.project==headers[2]:
                obj.attendance_month=result[2]
            elif obj.project==headers[3]:
                obj.attendance_month=result[3]
            elif obj.project==headers[4]:
                obj.attendance_month=result[4]
            elif obj.project==headers[5]:
                obj.attendance_month=result[5]
            elif obj.project==headers[6]:
                obj.attendance_month=result[6]
            elif obj.project==headers[7]:
                obj.attendance_month=result[7]
            else:
                obj.attendance_month=result[8]

            obj.save()
        else:
            return 0

    def sum_perple_month(self,obj):
        total_months=0
        sum_months=obj.attendance_month
        for month in sum_months:
            total_months=total_months+month
        return total_months

    def export_as_excel(self, request, queryset):
        meta = self.model._meta
        fields_name = []
        for field in meta.fields:
            if field.name == 'project':
                field_name = '项目组名称'
            elif field.name == 'order_month':
                field_name = '订单人月'
            elif field.name == 'service_people':
                field_name = '服务人员数量'
            elif field.name == 'sum_perple_month':
                field_name = '累计投入人月'
            elif field.name == 'remianing_month':
                field_name = '剩余投入人月'
            elif field.name == 'attendance_month':
                field_name = '已考勤人月合计'
            else:
                continue
            fields_name.append(field_name)
        response=HttpResponse(content_type='application/msexcel')
        response['Content-Disposition'] = f'attachment; filename={meta}.xlsx'
        wb = Workbook()
        ws = wb.active
        ws.append(fields_name)
        for obj in queryset:
            data=[]
            for field in fields_name:
                if field=='项目组名称':
                    res = obj.project
                elif field == '订单人月':
                    res = obj.order_month
                elif field == '服务人员数量':
                    res = obj.service_people
                elif field == '累计投入人月':
                    res = obj.sum_perple_month
                elif field== '剩余投入人月':
                    res=obj.remaining_month
                elif field == '已考勤人月合计':
                    res = obj.attendance_month
                else:
                    continue
                data.append(res)
            ws.append(data)

        wb.save(response)
        return  response

    export_as_excel.short_description = '导出Excel'


class ProjectMonthResource(resources.ModelResource):
    project = fields.Field(column_name='project', attribute='project')
    entrance_inform = fields.Field(column_name='entrance_inform', attribute='entrance_inform')
    entrance_date = fields.Field(column_name='entrance_date',attribute='entrance_date')
    cycle = fields.Field(column_name='cycle', attribute='cycle')
    contract_month = fields.Field(column_name='contract_month', attribute='contract_month')
    complete_month = fields.Field(column_name='complete_month', attribute='complete_month')
    unfinished_month = fields.Field(column_name='unfinished_month', attribute='unfinished_month')
    people_sum = fields.Field(column_name='people_sum', attribute='people_sum')
    expect_time = fields.Field(column_name='expect_time', attribute='expect_time')
    end_time = fields.Field(column_name='end_time', attribute='end_time')

    class Meta:
        model = ProjectMonth
        fields = ()
        import_id_fields = ('project', 'entrance_inform', 'entrance_date','cycle','contract_month','complete_month','unfinished_month','people_sum','expect_time','end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            #id=row['id']
            project = row['project']
            entrance_inform = row['entrance_inform']
            entrance_date = row['entrance_date']
            cycle= row['cycle']
            contract_month=row['contract_month']
            complete_month= row['complete_month']
            unfinished_month = row['unfinished_month']
            people_sum = row['people_sum']
            expect_time = row['expect_time']
            end_time = row['end_time']

            if project is None or entrance_inform is None or entrance_date is None or cycle is None or contract_month is None or complete_month is None \
                    or unfinished_month is None or people_sum is None or expect_time is None or end_time is None:
                continue
            #try:
                #project_name = Team.objects.get(id=int(id))
            #except Team.DoesNotExist:
                #error += '项目组{0}不存在！'.format(str(id))
            try:
                projectmonth= ProjectMonth.objects.get(project=project)
                #team.project_id=int(id)
                projectmonth.project = project
                projectmonth.entrance_inform = entrance_inform
                projectmonth.entrance_date = entrance_date
                projectmonth.cycle = cycle
                projectmonth.contract_month = contract_month
                projectmonth.complete_month=complete_month
                projectmonth.unfinished_month = unfinished_month
                projectmonth.people_sum = people_sum
                projectmonth.expect_time = expect_time
                projectmonth.end_time = end_time
                project.save()
            except Project.DoesNotExist:
                Project.objects.create(project=project,
                                       entrance_inform=entrance_inform,
                                       entrance_date=entrance_date,
                                       cycle=cycle,
                                       contract_month=contract_month,
                                       complete_month=complete_month,
                                       unfinished_month=unfinished_month,
                                       people_sum=people_sum,
                                       expect_time=expect_time,
                                       end_time=end_time)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class ProjectMonthAdmin(ImportMixin, admin.ModelAdmin):
    actions = ['export_as_excel']
    list_display = ('project', 'entrance_inform', 'entrance_date', 'cycle', 'contract_month', 'complete_month',
                    'unfinished_month', 'people_sum', 'expect_time', 'end_time')
    search_fields = ('project',)
    ordering = ['project']
    save_on_top = True
    list_per_page = 50
    resource_class = ProjectMonthResource

    def expect_time(self,obj):
        expect_time=parser.parse(str(obj.entrance_date))+relativedelta(months=obj.contract_month)
        return expect_time

    def complete_month(self,obj):
        projectresource=ProjectResource.objects.all()
        for project_name in projectresource.project:
            for name in obj.project:
                if name==project_name:
                    obj.object.filter(project=name).complete_month=projectresource.object.filter(project=project_name).attendance_month
                else:
                    pass
        obj.save()

    def unfinished_month(self,obj):
        return round(obj.contract_month - obj.complete_month, 1)

    def end_time(self,obj):
        today=datetime.date.today()
        for time in obj.expect_time:
            if today-time:
                return 0
            else:
                return obj.expect_time
        obj.save()

    def export_as_excel(self, request, queryset):
        meta = self.model._meta
        fields_name = []
        for field in meta.fields:
            if field.name == 'project':
                field_name = '项目组'
            elif field.name == 'entrance_inform':
                field_name = '项目入场通知时间'
            elif field.name == 'entrance_date':
                field_name = '项目入场时间'
            elif field.name == 'cycle':
                field_name = '项目周期(月)'
            elif field.name == 'contract_month':
                field_name = '合同总人月数'
            elif field.name == 'complete_month':
                field_name = '已完成人月'
            elif field.name == 'unfinished_month':
                field_name = '未完成人月'
            elif field.name == 'people_sum':
                field_name = '项目人数'
            elif field.name == 'expect_time':
                field_name = '预计完成时间'
            elif field.name == 'end_time':
                field_name = '人月完成截止时间'
            else:
                continue
            fields_name.append(field_name)
        response=HttpResponse(content_type='application/msexcel')
        response['Content-Disposition'] = f'attachment; filename={meta}.xlsx'
        wb = Workbook()
        ws = wb.active
        ws.append(fields_name)
        for obj in queryset:
            data=[]
            for field in fields_name:
                if field=='项目组':
                    res = obj.project
                elif field == '项目入场通知时间':
                    res = obj.entrance_inform
                elif field == '项目入场时间':
                    res = obj.entrance_date
                elif field == '项目周期(月)':
                    res = obj.cycle
                elif field== '合同总人月数':
                    res=obj.contract_month
                elif field == '已完成人月':
                    res = obj.complete_month
                elif field == '未完成人月':
                    res = obj.unfinished_month
                elif field == '项目人数':
                    res = obj.people_sum
                elif field== '预计完成时间':
                    res=obj.expect_time
                elif field == '人月完成截止时间':
                    res = obj.end_time
                else:
                    continue
                data.append(res)
            ws.append(data)

        wb.save(response)
        return  response

    export_as_excel.short_description = '导出Excel'


class PeopleResource(resources.ModelResource):
    name = fields.Field(column_name='name', attribute='name')
    company_name = fields.Field(column_name='company_name', attribute='company_name')
    mobile = fields.Field(column_name='mobile',attribute='mobile')
    work_year = fields.Field(column_name='work_year', attribute='work_year')
    qualification = fields.Field(column_name='qualification', attribute='qualification')
    responsibility = fields.Field(column_name='responsibility', attribute='responsibility')
    direction = fields.Field(column_name='direction', attribute='direction')

    class Meta:
        model = People
        fields = ()
        import_id_fields = ('name', 'company_name', 'mobile','work_year','qualification','responsibility','direction')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            #id=row['id']
            name = row['name']
            company_name = row['company_name']
            mobile = row['mobile']
            work_year= row['work_year']
            qualification=row['qualification']
            responsibility= row['responsibility']
            direction = row['direction']

            if name is None or company_name is None or mobile is None or work_year is None or qualification is None or responsibility is None \
                    or direction is None:
                continue
            #try:
                #project_name = Team.objects.get(id=int(id))
            #except Team.DoesNotExist:
                #error += '项目组{0}不存在！'.format(str(id))
            try:
                people= People.objects.get(name=name)
                #team.project_id=int(id)
                people.name = name
                people.company_name = company_name
                people.mobile = mobile
                people.work_year = work_year
                people.qualification = qualification
                people.responsibility=responsibility
                people.direction = direction
                people.save()
            except People.DoesNotExist:
                People.objects.create(name=name,
                                       company_name=company_name,
                                       mobile=mobile,
                                       work_year=work_year,
                                       qualification=qualification,
                                       responsibility=responsibility,
                                       direction=direction)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('name', 'company_name', 'mobile', 'work_year', 'qualification', 'responsibility',
                    'direction')
    search_fields = ('name',)
    ordering = ['name']
    save_on_top = True
    list_per_page = 50
    resource_class = PeopleResource


class PeopleJobsResource(resources.ModelResource):
    people = fields.Field(column_name='people', attribute='people')
    project = fields.Field(column_name='project', attribute='project')
    jobs_day = fields.Field(column_name='jobs_day',attribute='jobs_day')

    class Meta:
        model = PeopleJobs
        fields = ()
        import_id_fields = ('people', 'project', 'jobs_day')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            people = row['people']
            project = row['project']
            jobs_day = row['jobs_day']

            if people is None or project is None or jobs_day is None :
                continue
            try:
                peoplejob= PeopleJobs.objects.get(people=people)
                peoplejob.people = people
                peoplejob.project = project
                peoplejob.jobs_day = jobs_day
                peoplejob.save()
            except PeopleJobs.DoesNotExist:
                PeopleJobs.objects.create(people=people,
                                       project=project,
                                       jobs_day=jobs_day)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleJobsAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('people', 'project', 'jobs_day')
    search_fields = ('people',)
    ordering = ['project']
    save_on_top = True
    list_per_page = 50
    resource_class = PeopleJobsResource


class PeopleGroupingResource(resources.ModelResource):
    project = fields.Field(column_name='project', attribute='project')
    people = fields.Field(column_name='people', attribute='people')
    start_time = fields.Field(column_name='start_time',attribute='start_time')
    end_time = fields.Field(column_name='end_time', attribute='end_time')

    class Meta:
        model = PeopleGrouping
        fields = ()
        import_id_fields = ('project', 'people', 'start_time','end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            project = row['project']
            people = row['people']
            start_time = row['start_time']
            end_time = row['end_time']

            if people is None or project is None or start_time is None or end_time is None:
                continue
            try:
                peoplegroup= PeopleGrouping.objects.get(people=people)
                peoplegroup.project = project
                peoplegroup.people = people
                peoplegroup.start_time = start_time
                peoplegroup.end_time = end_time
                peoplegroup.save()
            except PeopleGrouping.DoesNotExist:
                PeopleGrouping.objects.create(project=project,
                                              people=people,
                                              start_time=start_time,
                                              end_time=end_time)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleGroupingAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('project', 'people','start_time','end_time')
    search_fields = ('people',)
    ordering = ['project']
    save_on_top = True
    list_per_page = 50
    resource_class = PeopleGroupingResource


class PeopleEntranceResource(resources.ModelResource):
    people = fields.Field(column_name='people', attribute='people')
    entrance_type = fields.Field(column_name='entrance_type', attribute='entrance_type')
    team = fields.Field(column_name='team',attribute='team')
    project = fields.Field(column_name='project', attribute='project')
    entrance_date = fields.Field(column_name='entrance_date', attribute='entrance_date')
    entrance_note = fields.Field(column_name='entrance_note', attribute='entrance_note')
    end_time = fields.Field(column_name='end_time', attribute='end_time')

    class Meta:
        model = PeopleEntrance
        fields = ()
        import_id_fields = ('people', 'entrance_type', 'team','project','entrance_date','entrance_note','end_time')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            people = row['people']
            entrance_type = row['entrance_type']
            team = row['team']
            project = row['project']
            entrance_date = row['entrance_date']
            entrance_note = row['entrance_note']
            end_time = row['end_time']

            if people is None or entrance_type is None or team is None or project is None or entrance_date is None \
                    or entrance_note is None or end_time is None:
                continue
            try:
                people_entrance= PeopleEntrance.objects.get(people=people)
                people_entrance.people = people
                people_entrance.entrance_type = entrance_type
                people_entrance.team = team
                people_entrance.project = project
                people_entrance.entrance_date = entrance_date
                people_entrance.entrance_note = entrance_note
                people_entrance.end_time = end_time
                people_entrance.save()
            except PeopleEntrance.DoesNotExist:
                PeopleEntrance.objects.create(people=people,
                                              entrance_type=entrance_type,
                                              team=team,
                                              project=project,
                                              entrance_date=entrance_date,
                                              entrance_note=entrance_note,
                                              end_time=end_time)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleEntranceAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('people', 'entrance_type','team', 'project','entrance_date','entrance_note','end_time')
    search_fields = ('people',)
    ordering = ['project']
    save_on_top = True
    list_per_page = 50
    resource_class = PeopleEntranceResource


class PeopleIdleResource(resources.ModelResource):
    people = fields.Field(column_name='people', attribute='people')
    idea_date = fields.Field(column_name='idea_date', attribute='idea_date')
    note = fields.Field(column_name='note',attribute='note')

    class Meta:
        model = PeopleIdle
        fields = ()
        import_id_fields = ('people', 'idea_date', 'note')

    def import_data(self, dataset, dry_run=False, raise_errors=False,
                    use_transactions=None, **kwargs):
        result = self.get_result_class()()
        result.diff_headers = self.get_diff_headers()
        result.totals = OrderedDict([(RowResult.IMPORT_TYPE_NEW, 0),
                                     (RowResult.IMPORT_TYPE_UPDATE, 0),
                                     (RowResult.IMPORT_TYPE_DELETE, 0),
                                     (RowResult.IMPORT_TYPE_SKIP, 0),
                                     (RowResult.IMPORT_TYPE_ERROR, 0),
                                     ('total', len(dataset))])
        instance_loader = self._meta.instance_loader_class(self, dataset)
        result.totals['total'] = len(dataset)
        for row in dataset.dict:
            error = ''
            people = row['people']
            idea_date = row['idea_date']
            note = row['note']

            if people is None or idea_date is None or note is None:
                continue
            try:
                people_idle= PeopleIdle.objects.get(people=people)
                people_idle.people = people
                people_idle.idea_date = idea_date
                people_idle.note = note
                people_idle.save()
            except PeopleIdle.DoesNotExist:
                PeopleIdle.objects.create(people=people,
                                          idea_date=idea_date,
                                          note=note)
            row_result = self.import_row(
                row, instance_loader, dry_run, **kwargs)
            if row_result.errors:
                if error:
                    row_result.errors = []
                    row_result.errors.append(
                        self.get_error_result_class()(error, '', row))
                result.totals[row_result.IMPORT_TYPE_ERROR] += 1
            if (row_result.import_type != RowResult.IMPORT_TYPE_SKIP or
                    self._meta.report_skipped):
                result.rows.append(row_result)
        return result


class PeopleIdleAdmin(ImportMixin , admin.ModelAdmin):
    list_display = ('people', 'idea_date', 'note')
    search_fields = ('people',)
    ordering = ['people']
    save_on_top = True
    list_per_page = 50
    resource_class = PeopleIdleResource


admin.site.register(Team, TeamAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectResource, ProjectResourceAdmin)
admin.site.register(ProjectMonth, ProjectMonthAdmin)
admin.site.register(People, PeopleAdmin)
admin.site.register(PeopleJobs, PeopleJobsAdmin)
admin.site.register(PeopleGrouping, PeopleGroupingAdmin)
admin.site.register(PeopleEntrance, PeopleEntranceAdmin)
admin.site.register(PeopleIdle, PeopleIdleAdmin)