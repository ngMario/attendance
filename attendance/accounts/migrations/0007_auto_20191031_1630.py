# Generated by Django 2.2.6 on 2019-10-31 08:30

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_remove_attendance_project_group'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendance',
            name='attendance_date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='考勤时间'),
        ),
        migrations.AlterField(
            model_name='attendance',
            name='back_date',
            field=models.TimeField(verbose_name='签退时间'),
        ),
        migrations.AlterField(
            model_name='attendance',
            name='in_date',
            field=models.TimeField(verbose_name='签到时间'),
        ),
    ]
