from django.apps import AppConfig


class ProjectManagementConfig(AppConfig):
    name = 'project_management'
    verbose_name = '用工系统'
