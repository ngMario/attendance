# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/3/27 15:36
# @File    : admin.py
from __future__ import unicode_literals

from collections import OrderedDict
from datetime import datetime, timedelta, date
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.db.models import Sum, Q
from import_export.admin import ImportMixin, ExportMixin, ImportExportModelAdmin
from import_export import resources, fields
from import_export.widgets import ManyToManyWidget, ForeignKeyWidget
from import_export.results import Result, RowResult

from .models import *
import people_month.utils as utils
import people_month.get_html as get_html
import people_month.import_excel as import_excel


TIME_YEAR = datetime.strftime(datetime.now(), "%Y")


class ReadOnlyModelAdmin(admin.ModelAdmin):
    """
    ModelAdmin class that prevents modifications through the admin.

    The changelist and the detail view work, but a 403 is returned
    if one actually tries to edit an object.
    """

    actions = None

    def get_readonly_fields(self, request, obj=None):
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        if request.method not in ('GET', 'HEAD'):
            return False
        return super(ReadOnlyModelAdmin, self).has_change_permission(request, obj)

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    """
    供应商admin
    """
    list_display = ('id', 'name', 'note', 'created_at', 'modified_at')
    search_fields = ('name', 'note')
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50


@admin.register(Team)
class TeamAdmin(ImportMixin, admin.ModelAdmin):
    """
    团队admin
    """
    list_display = ('id', 'team_name', 'team_note', 'team_head', 'team_type', 'start_time', 
                    'end_time', 'created_at', 'modified_at')
    search_fields = ('team_name', 'team_head')
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50
    resource_class = import_excel.TeamResource


@admin.register(ContractProject)
class ProjectAdmin(ImportMixin, admin.ModelAdmin):
    """
    合同项目admin
    """
    list_display = ('id', 'team', 'project_name', 'project_note', 'project_type', 'bank_head', 'company_head',
                    'cycle', 'notice_time', 'start_time', 'contract_month', 'end_time', 'senior_month', 'experts_month',
                    'high_month', 'intermediate_month', 'primary_month', 'created_at', 'modified_at'
                    )
    search_fields = ('project_name', )
    autocomplete_fields = ['team']
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50
    resource_class = import_excel.ContractProjectResource


@admin.register(ContractProjectBaseInfo)
class ContractProjectBaseInfoAdmin(ReadOnlyModelAdmin):
    """
    项目人月基本情况admin
    """
    list_display = ('id', 'project_name', 'team_name', 'notice_time', 'start_time',
                    'contract_month','old_experts', 'experts', 'senior', 'intermediate', 'primary',
                    'sum_input_month', 'remaining', 'server_people', 'completion_time', 'completion_time_month',
                    )
    search_fields = ('project_name',)
    autocomplete_fields = ['team']
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50

    def team_name(self, obj):
        return obj.team.team_name

    def old_experts(self, obj):
        return utils.old_experts(obj)

    def experts(self, obj):
        return utils.experts(obj)

    def senior(self, obj):
        return utils.senior(obj)

    def intermediate(self, obj):
        return utils.intermediate(obj)

    def primary(self, obj):
        return utils.primary(obj)

    def sum_input_month(self, obj):
        return utils.sum_input_month(obj)

    def remaining(self, obj):
        return utils.remaining(obj)

    def server_people(self, obj):
        return utils.server_people(obj)

    def completion_time(self, obj):
        server_people = utils.server_people(obj)
        if server_people == 0:
            old_date = obj.contract_month * 22
        else:
            old_date = (obj.contract_month / server_people) * 22
        if obj.start_time is None:
            return ''
        else:
            return datetime.strftime(obj.start_time + timedelta(days=int(old_date)), "%Y-%m-%d")

    def completion_time_month(self, obj):
        if obj.start_time is None or obj.cycle is None:
            return ''
        else:
            return datetime.strftime(obj.start_time + timedelta(days=int(obj.cycle * 30)), "%Y-%m-%d")

    team_name.short_description = '团队'
    old_experts.short_description = '资深专家'
    experts.short_description = '专家'
    senior.short_description = '高级'
    intermediate.short_description = '中级'
    primary.short_description = '初级'
    sum_input_month.short_description = '已完成总人月'
    remaining.short_description = '未完成总人月'
    server_people.short_description = '项目人数'
    completion_time.short_description = '预计完成时间'
    completion_time_month.short_description = '人月完成截止时间'


@admin.register(ContractProjecStatistical)
class ContractProjectStatisticalAdmin(ReadOnlyModelAdmin):
    """
    项目人月基本情况admin
    """
    list_display = ('id', 'get_html_return',
                    )
    search_fields = ('project_name', )
    autocomplete_fields = ['team']
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50

    def get_html_return(self, obj):
        # 资深专家
        old_experts_total_day, old_experts_month, old_experts_ps, old_differences,\
        old_experts, old_need, old_complete = get_html.old_experts_html(obj)

        # 专家
        experts_total_day, experts_month, experts_ps, experts_differences, experts, experts_need,\
        experts_complete = get_html.experts_html(obj)

        # 高级
        senior_total_day, senior_month, senior_ps, senior_differences, senior,\
        senior_need, senior_complete = get_html.senior_html(obj)

        # 中级
        intermediate_total_day, intermediate_month, intermediate_ps, intermediate_differences, intermediate, \
        intermediate_need, intermediate_complete = get_html.intermediate_html(obj)

        # 初级
        primary_total_day, primary_month, primary_ps, primary_differences, primary, \
        primary_need, primary_complete = get_html.primary_html(obj)

        v0 = datetime.today().strftime("%Y-%m-%d")
        v1 = obj.project_name
        v2 = obj.notice_time
        v3 = obj.cycle
        v = '数据截止日期:{0}<br>' \
            '合同项目:{1}<br>' \
            '进场通知:{2},项目周期{3}个月<br>'.format(v0, v1, v2, v3)
        res = TheJobInfo.objects.filter(info_project_name=obj.id).order_by(
            '-people__admission_qualification', 'people__name')
        count = 1
        tt = ''
        res_name = []
        for re in res:
            name = re.people.name
            if name in res_name:
                continue
            admission_qualification = re.people.admission_qualification
            job_info = TheJobInfo.objects.filter(
                people=re.people, info_project_name=obj.id).values('jobs_day').annotate(
                num_jobs_day=Sum('jobs_day'))
            try:
                info = job_info[0]['num_jobs_day']
            except:
                info = 0
            if admission_qualification == 0:
                adm = '初级工程师'
            elif admission_qualification == 1:
                adm = '中级工程师'
            elif admission_qualification == 2:
                adm = '高级工程师'
            elif admission_qualification == 3:
                adm = '专家'
            elif admission_qualification == 4:
                adm = '资深专家'
            else:
                adm = '其他'
            tt += '<tr>' \
                  '<th>{0}</th>' \
                  '<th>{1}</th>' \
                  '<th>{2}</th>' \
                  '<th>{3}</th>' \
                  '</tr>'.format(count, name, adm, info)
            res_name.append(name)
            count += 1

        t = '<br>' \
            '<table border="1">  <tr>    ' \
            '<th>序号</th>    ' \
            '<th>姓名</th>  ' \
            '<th>进场级别</th>  ' \
            '<th>合计天数</th> </tr>' + tt + ' </table> ' \
            '<br>'
        s = mark_safe(v +
                      '<table border="1">  '
                      '<tr>    '
                      '<th></th>    '
                      '<th>总天数</th>  '
                      '<th>人月数</th>  '
                      '<th>合同数</th>  '
                      '<th>差异</th>  '
                      '<th>在职人数</th>  '
                      '<th>按现有人数打卡仍需/月</th>  '
                      '<th>预计完成时间</th>  '
                      '</tr>  '
                      '<tr>    '
                      '<td>资深专家</td>    '
                      '<td>{0}</td>    '
                      '<td>{1}</td>    '
                      '<td>{2}</td>    '
                      '<td>{3}</td>    '
                      '<td>{4}</td>    '
                      '<td>{5}</td>    '
                      '<td>{6}</td>    '
                      '</tr>  '
                      '<tr>    '
                      '<td>专家</td>    '
                      '<td>{7}</td>    '
                      '<td>{8}</td>    '
                      '<td>{9}</td>    '
                      '<td>{10}</td>    '
                      '<td>{11}</td>    '
                      '<td>{12}</td>    '
                      '<td>{13}</td>    '
                      '</tr>  '
                      '<tr>    '
                      '<td>高级工程师</td>    '
                      '<td>{14}</td>    '
                      '<td>{15}</td>    '
                      '<td>{16}</td>    '
                      '<td>{17}</td>    '
                      '<td>{18}</td>    '
                      '<td>{19}</td>    '
                      '<td>{20}</td>    '
                      '</tr>  '
                      '<tr>    '
                      '<td>中级工程师</td>    '
                      '<td>{21}</td>    '
                      '<td>{22}</td>    '
                      '<td>{23}</td>    '
                      '<td>{24}</td>    '
                      '<td>{25}</td>    '
                      '<td>{26}</td>    '
                      '<td>{27}</td>    '
                      '</tr>  '
                      '<tr>    '
                      '<td>初级工程师</td>    '
                      '<td>{28}</td>    '
                      '<td>{29}</td>    '
                      '<td>{30}</td>    '
                      '<td>{31}</td>    '
                      '<td>{32}</td>    '
                      '<td>{33}</td>    '
                      '<td>{34}</td>    '
                      '</tr></table>'.format(old_experts_total_day, old_experts_month, old_experts_ps, old_differences,
                                             old_experts, old_need, old_complete, experts_total_day, experts_month,
                                             experts_ps, experts_differences, experts, experts_need, experts_complete,
                                             senior_total_day, senior_month, senior_ps, senior_differences, senior,
                                             senior_need, senior_complete,intermediate_total_day, intermediate_month,
                                             intermediate_ps, intermediate_differences, intermediate, intermediate_need,
                                             intermediate_complete, primary_total_day, primary_month, primary_ps,
                                             primary_differences, primary, primary_need, primary_complete)+t)

        return s

    get_html_return.allow_tags = True
    get_html_return.short_description = '内容'


@admin.register(ContractProjectBase)
class ContractProjectBaseAdmin(ReadOnlyModelAdmin):
    """
    团队资源使用情况admin
    """
    list_display = ('id', 'project_name', 'bank_head', 'company_head', 'contract_month',
                    'server_people', 'sum_input_month', 'remaining', 'input_month')
    search_fields = ('project_name', )
    autocomplete_fields = ['team']
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50

    def contract_month(self, obj):
        return obj.contract_month

    def server_people(self, obj):
        return utils.server_people(obj)
    
    def sum_input_month(self, obj):
        return utils.sum_input_month(obj)
    
    def remaining(self, obj):
        return utils.remaining(obj)
    
    def input_month(self, obj):
        return utils.input_month(obj)

    server_people.short_description = '服务人员数量'
    sum_input_month.short_description = '已投入人月'
    remaining.short_description = '剩余投入人月'
    input_month.short_description = '已考勤人月'
    contract_month.short_description = '订单人月'


@admin.register(People)
class PeopleAdmin(ImportMixin, admin.ModelAdmin):
    """
    公司人员admin
    """
    list_display = ('id', 'name', 'company_name', 'mobile', 'email', 'entry_time', 'work_year', 'qualification',
                    'direction', 'admission_qualification', 'schooling', 'state', 'resign_date', 'responsibility',
                    'created_at', 'modified_at', )
    search_fields = ('name', )
    autocomplete_fields = ['company_name']
    ordering = ['id']
    save_on_top = True
    list_per_page = 50
    resource_class = import_excel.PeopleResource

    def work_year(self, obj):
        return utils.work_year(obj.graduation_time)
    
    def qualification(self, obj):
        return utils.qualification(obj.schooling, obj.graduation_time)
    
    work_year.short_description = '工作年限(年)'
    qualification.short_description = '人员资质'


@admin.register(IdlePeople)
class IdlePeopleAdmin(ReadOnlyModelAdmin):
    list_display = ('id', 'name', 'total', 'notes')
    search_fields = ('name',)
    autocomplete_fields = ['company_name']
    ordering = ['id']
    save_on_top = True
    list_per_page = 50

    def total(self, obj):
        total, s = utils.return_total_s(obj)

        return total

    def notes(self, obj):
        total, s = utils.return_total_s(obj)
        return s

    def get_queryset(self, request):
        qs = People.objects.filter(~Q(state=1))
        return qs

    total.short_description = '合计'
    notes.short_description = '描述'


@admin.register(TheJobInfo)
class TheJobInfoAdmin(ImportMixin, admin.ModelAdmin):
    """
    在岗天数（原始数据）admin
    """
    list_display = ('id', 'people', 'jobs_day', 'in_month', 'info_project_name', 'team', 'created_at')
    search_fields = ('people__name', 'info_project_name__project_name')
    autocomplete_fields = ['people']
    ordering = ['-created_at']
    save_on_top = True
    list_per_page = 50
    resource_class = import_excel.TheJobInfoResource

    def in_month(self, obj):
        return obj.created_at.strftime("%Y-%m")

    in_month.short_description = '月份'


@admin.register(PeopleContractProject)
class PeopleContractProjectAdmin(ImportMixin, admin.ModelAdmin):
    """
    合同项目人员投入关系表admin
    """
    list_display = ('bank_head', 'contract_project', 'people', 'mobile', 'work_year', 'qualification',
                    'start_time', 'end_time', 'company_name', 'direction', 'is_start', )
    search_fields = ('people__name', 'contract_project__project_name')
    autocomplete_fields = ['people', 'contract_project', ]
    ordering = ['-id']
    save_on_top = True
    list_per_page = 50
    resource_class = import_excel.PeopleContractProjectResource

    def bank_head(self, obj):
        return obj.contract_project.bank_head

    def mobile(self, obj):
        return obj.people.mobile

    def work_year(self, obj):
        return utils.work_year(obj.people.graduation_time)

    def qualification(self, obj):
        return utils.qualification(obj.people.schooling, obj.people.graduation_time)

    def company_name(self, obj):
        return obj.people.company_name.name

    def direction(self, obj):
        return obj.people.direction

    bank_head.short_description = '小组负责人'
    mobile.short_description = '联系方式'
    work_year.short_description = '工作年限'
    qualification.short_description = '人员资质'
    company_name.short_description = '公司'
    direction.short_description = '专业方向'
