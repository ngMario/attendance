# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2019/10/28 15:48
# @File    : api_urls.py
from __future__ import unicode_literals

from accounts import api
from attendance.router import router

router.register(r'accounts', api.AttendanceViewSet, 'accounts_attendance')
router.register(r'position', api.ModifePositionViewSet, 'modife_position')
router.register(r'accounts', api.RegisterednViewSet, 'register')

urlpatterns = [
]
