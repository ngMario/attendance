# # -*- coding:utf-8 -*-
# # author：Anson
# # @Time    : 2020/3/27 15:36
# # @File    : admin.py
# from __future__ import unicode_literals

# from datetime import datetime, date

# from collections import OrderedDict
# from django.contrib import admin
# from django.db.models import Sum
# from import_export.admin import ImportMixin, ExportMixin, ImportExportModelAdmin
# from import_export import resources, fields
# from import_export.widgets import ManyToManyWidget, ForeignKeyWidget
# from import_export.results import Result, RowResult

# from .models import *
# from .resources_admin import *


# @admin.register(Team)
# class TeamAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     团队admin
#     """
#     list_display = ('id', 'team_name', 'team_note', 'team_head', 'team_type', 'start_time', 'end_time', 'created_at', 'modified_at')
#     search_fields = ('team_name', 'team_head')
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50


# @admin.register(Project)
# class ProjectAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     项目admin
#     """
#     list_display = ('id', 'team', 'project_name', 'project_note', 'project_type', 'bank_head', 'company_head',
#                     'start_time', 'end_time', 'created_at', 'modified_at')
#     search_fields = ('project_name', 'company_head')
#     autocomplete_fields = ['team']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50


# @admin.register(ProjectResource)
# class ProjectResourceAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     团队项目资源使用情况admin
#     """
#     list_display = ('id', 'project', 'bank_head', 'company_head', 'order_month', 'service_people', 'sum_perple_month', 'remaining_month',
#                     'attendance_month', 'created_at', 'modified_at')
#     autocomplete_fields = ['project']
#     search_fields = ('project', )
#     resource_class = ProjectResourceResource
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50

#     def bank_head(self, obj):
#         return obj.project.bank_head
    
#     def company_head(self, obj):
#         return obj.project.company_head

#     bank_head.short_description = '行方负责人'
#     company_head.short_description = '公司负责人'


# @admin.register(ProjectMonth)
# class ProjectMonthAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     项目人月基本情况admin
#     """
#     list_display = ('id', 'project', 'entrance_inform', 'entrance_date', 'cycle', 'contract_month',
#                     'complete_month', 'unfinished_month', 'people_sum', 'expect_time', 'end_time',
#                     'created_at', 'modified_at')
#     search_fields = ('project', )
#     autocomplete_fields = ['project']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50


# @admin.register(People)
# class PeopleAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     公司人员表admin
#     """
#     list_display = ('id', 'name', 'company_name', 'mobile', 'work_year', 'qualification',
#                     'responsibility', 'direction', 'created_at', 'modified_at')
#     search_fields = ('name', 'mobile')
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50

#     def work_year(self, obj):
#         w_year = date.today() - obj.graduation_time
#         return int(round(w_year.days / 365))
    
#     work_year.short_description = '工作年限(年)'


# @admin.register(PeopleJobs)
# class PeopleJobsAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     公司人员在岗情况admin
#     """
#     list_display = ('id', 'people', 'company_name', 'project', 'jobs_day', 'created_at', 'modified_at')
#     search_fields = ('people', )
#     autocomplete_fields = ['people', 'project']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50

#     def company_name(self, obj):
#         return obj.people.company_name

#     company_name.short_description = '供应商公司名称'


# @admin.register(PeopleGrouping)
# class PeopleGroupingAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     人员分组情况admin
#     """
#     list_display = ('id', 'group_leader', 'project', 'people', 'mobile', 'work_year',
#                     'qualification', 'responsibility', 'start_time', 'end_time', 'company_name',
#                     'created_at', 'modified_at')
#     search_fields = ('people', )
#     autocomplete_fields = ['people', 'project']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50

#     def group_leader(self, obj):
#         return obj.project.company_head

#     def mobile(self, obj):
#         return obj.people.mobile

#     def work_year(self, obj):
#         w_year = date.today() - obj.people.graduation_time
#         return int(round(w_year.days / 365))

#     def qualification(self, obj):
#         s_type = obj.people.qualification
#         if s_type == 0:
#             l_type = '初级'
#         elif s_type == 1:
#             l_type = '中级'
#         elif s_type == 2:
#             l_type = '高级'
#         elif s_type == 3:
#             l_type = '初级项目经理'
#         elif s_type == 4:
#             l_type = '中级项目经理'
#         elif s_type == 5:
#             l_type = '高级项目经理'
#         else:
#             l_type = '无'
#         return l_type

#     def responsibility(self, obj):
#         return obj.people.responsibility

#     def company_name(self, obj):
#         return obj.people.company_name

#     def direction(self, obj):
#         return obj.people.direction

#     group_leader.short_description = '小组负责人'
#     mobile.short_description = '联系方式'
#     work_year.short_description = '工作年限'
#     qualification.short_description = '人员资质'
#     responsibility.short_description = '工作职责'
#     company_name.short_description = '公司'
#     direction.short_description = '专业方向'


# class PeopleEntranceListLine(admin.TabularInline):
#     model = PeopleEntranceList

#     def formfield_for_dbfield(self, db_field, **kwargs):
#         field = super(PeopleEntranceListLine, self).formfield_for_dbfield(db_field, **kwargs)
#         return field
#     extra = 0 #默认显示条目的数量


# @admin.register(PeopleEntrance)
# class PeopleEntranceAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     人员入场情况表admin
#     """
#     inlines = [PeopleEntranceListLine, ]
#     resource_class = PeopleEntranceResource
#     list_display = ('id', 'people', 'entrance_type', 'team', 'project',
#                     'entrance_note', 'end_time', 'created_at', 'modified_at')
#     search_fields = ('people', )
#     autocomplete_fields = ['people', 'project', 'team']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50


# # @admin.register(PeopleEntranceList)
# # class PeopleEntranceListAdmin(admin.ModelAdmin):
# #     """
# #     人员入场情况详情表admin
# #     """
# #     list_display = ('id', 'people_entrance', 'entrance_date', 'project')
# #     search_fields = ('project', )
# #     ordering = ['-id']
# #     save_on_top = True
# #     list_per_page = 50


# class PeopleIdleListLine(admin.TabularInline):
#     model = PeopleIdleList

#     def formfield_for_dbfield(self, db_field, **kwargs):
#         field = super(PeopleIdleListLine, self).formfield_for_dbfield(db_field, **kwargs)
#         return field
#     extra = 0 #默认显示条目的数量


# @admin.register(PeopleIdle)
# class PeopleIdleAdmin(ImportMixin, admin.ModelAdmin):
#     """
#     人员闲置情况登记表admin
#     """
#     inlines = [PeopleIdleListLine, ]
#     resource_class = PeopleIdleResource
#     list_display = ('id', 'people', 'idle_day', 'note', 'entrance_type', 'created_at', 'modified_at')
#     search_fields = ('people', )
#     autocomplete_fields = ['people']
#     ordering = ['-id']
#     save_on_top = True
#     list_per_page = 50

#     def idle_day(self, obj):
#         sum_day = PeopleIdleList.objects.filter(people_idle=obj.id).aggregate(nums=Sum('idle_day'))
#         if sum_day['nums'] is not None:
#             ret = sum_day['nums']
#         else:
#             ret = 0
#         return ret

#     def entrance_type(self, obj):
#         try:
#             entrance = PeopleEntrance.objects.get(people=obj.people)
#             entrance_type = entrance.entrance_type
#         except:
#             entrance_type = 3
#         if entrance_type == 0:
#             ret = '入场'
#         elif entrance_type == 1:
#             ret = '未入场'
#         elif entrance_type == 2:
#             ret = '闲置'
#         else:
#             ret = '其他'
#         return ret

#     idle_day.short_description = '闲置天数合计'
#     entrance_type.short_description = '当前人员状态'


# # @admin.register(PeopleIdleList)
# # class PeopleIdleListAdmin(admin.ModelAdmin):
# #     list_display = ('id', 'people_idle', 'idle_date', 'idle_day', )
# #     search_fields = ('people_idle', )
# #     ordering = ['-id']
# #     save_on_top = True
# #     list_per_page = 50
