# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Accounts(models.Model):
    name = models.CharField('姓名', max_length=30, unique=True)
    email = models.EmailField('邮箱', unique=True, null=True, blank=True)
    id_card_no = models.CharField('身份证号', max_length=20, help_text='身份证号')
    mobile = models.CharField('手机号', max_length=100, null=True, blank=True)
    id_card_photo_front = models.ImageField(
        '身份证正面照片', null=True, blank=True, max_length=200,
        upload_to='id_verify/front/%Y/%m/%d/%H/%M%S', help_text='身份证正面照片')
    id_card_photo_back = models.ImageField(
        '身份证反面照片', null=True, blank=True, max_length=200,
        upload_to='id_verify/back/%Y/%m/%d/%H/%M%S', help_text='身份证反面照片')
    STATUS = (
        (0, '审核中'),
        (1, '已认证'),
        (2, '审核失败'),
    )
    status = models.SmallIntegerField('状态', choices=STATUS, default=0)
    status_note = models.CharField(
        '状态描述', max_length=200, null=True, blank=True,
        help_text='审核记录, 当审核失败时，给出失败理由')

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = '用户资料'
        verbose_name_plural = '用户资料'


class WithdrawalRecord(models.Model):
    account = models.ForeignKey(Accounts, verbose_name='用户', related_name='account_record', on_delete=models.CASCADE)
    bank_card_no = models.CharField(max_length=20, verbose_name='银行卡号')
    bank_name = models.CharField(max_length=50, verbose_name='银行名称')
    amount = models.DecimalField('提现金额', max_digits=12, decimal_places=2, default=0)
    STATE = (
        (0, '处理中'),
        (1, '成功'),
        (-1, '失败'),
    )
    status = models.SmallIntegerField(choices=STATE, default=0, help_text='状态')
    note = models.CharField('备注(提现失败原因)', max_length=2000, null=True, blank=True)

    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    class Meta:
        verbose_name = '提现记录'
        verbose_name_plural = verbose_name


