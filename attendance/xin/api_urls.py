# -*- coding:utf-8 -*-
# author：Anson
# @Time    : 2020/1/14 10:21
# @File    : api_urls.py
from __future__ import unicode_literals

from xin import api
from attendance.router import router

router.register(r'xin', api.AppointmentViewSet, 'xin_add')

urlpatterns = [
]