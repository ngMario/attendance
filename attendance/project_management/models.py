# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.


class Team(models.Model):
    team_name = models.CharField('团队名称', max_length=50)
    team_note = models.CharField('团队描述', max_length=2000, null=True, blank=True)
    TEAM_TYPE = (
        (0, '未开始'),
        (1, '已开始'),
        (2, '结束'),
        (3, '未知'),
    )
    team_head = models.CharField('团队负责人', max_length=50, null=True, blank=True)
    team_type = models.SmallIntegerField('团队状态', choices=TEAM_TYPE, default=0)
    start_time = models.DateField('开始时间', null=True, blank=True)
    end_time = models.DateField('结束时间', null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.team_name

    class Meta:
        verbose_name = '团队'
        verbose_name_plural = verbose_name


class Project(models.Model):
    team = models.ForeignKey(Team, related_name='project_team', on_delete=models.CASCADE,
                             verbose_name='团队')
    project_name = models.CharField('项目名称', max_length=50)
    project_note = models.CharField('项目描述', max_length=2000, null=True, blank=True)
    PROJECT_TYPE = (
        (0, '未开始'),
        (1, '已开始'),
        (2, '结束'),
        (3, '未知'),
    )
    project_type = models.SmallIntegerField('项目状态', choices=PROJECT_TYPE, default=0)
    bank_head = models.CharField('行方负责人', max_length=10, null=True, blank=True)
    company_head = models.CharField('公司负责人', max_length=10, null=True, blank=True)
    start_time = models.DateField('开始时间', null=True, blank=True)
    end_time = models.DateField('结束时间', null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.project_name

    class Meta:
        verbose_name = '项目'
        verbose_name_plural = verbose_name


class ProjectResource(models.Model):
    project = models.ForeignKey(Project, related_name='project_resource', on_delete=models.CASCADE,
                                verbose_name='项目')
    order_month = models.FloatField('订单人月', default=0, null=True, blank=True)
    service_people = models.IntegerField('服务人员数量', default=0, null=True, blank=True)
    sum_perple_month = models.FloatField('累计已投入人月合计', default=0, null=True, blank=True)
    remaining_month = models.FloatField('剩余投入人月', default=0, null=True, blank=True)
    attendance_month = models.FloatField('已考勤人月合计', default=0, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.project

    class Meta:
        verbose_name = '团队项目资源使用情况'
        verbose_name_plural = verbose_name


class ProjectMonth(models.Model):
    project = models.ForeignKey(Project, related_name='project_month', on_delete=models.CASCADE,
                                verbose_name='项目')
    entrance_inform = models.DateField('项目入场通知时间', null=True, blank=True)
    entrance_date = models.DateField('项目入场时间', null=True, blank=True)
    cycle = models.CharField('项目周期(月)', default='0', null=True, blank=True, max_length=50)
    contract_month = models.FloatField('合同总人月数', default=0, null=True, blank=True)
    complete_month = models.FloatField('已完成人月', default=0, null=True, blank=True)
    unfinished_month = models.FloatField('未完成人月', default=0, null=True, blank=True)
    people_sum = models.IntegerField('项目人数', default=0, null=True, blank=True)
    expect_time = models.DateField('预计完成时间', null=True, blank=True)
    end_time = models.DateField('人月完成截止时间', null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.entrance_inform

    class Meta:
        verbose_name = '项目人月基本情况'
        verbose_name_plural = verbose_name


class People(models.Model):
    name = models.CharField('姓名', max_length=50)
    company_name = models.CharField('供应商公司名称', max_length=50, default='成都天用唯勤科技股份有限公司',
                                    null=True, blank=True)
    mobile = models.CharField('联系方式', max_length=15, null=True, blank=True)
    graduation_time = models.DateField('毕业时间', default=timezone.now)
    # work_year = models.IntegerField('工作年限', default=0, null=True, blank=True)
    TYPE = (
        (0, '初级'),
        (1, '中级'),
        (2, '高级'),
        (3, '初级项目经理'),
        (4, '中级项目经理'),
        (5, '高级项目经理'),
        (6, '无'),
    )
    qualification = models.SmallIntegerField('人员资质', choices=TYPE, default=0)
    responsibility = models.CharField('工作职责', max_length=200, null=True, blank=True)
    direction = models.CharField('专业方向', max_length=200, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = '公司人员表'
        verbose_name_plural = verbose_name


class PeopleJobs(models.Model):
    people = models.ForeignKey(People, related_name='people_jobs', on_delete=models.CASCADE,
                               verbose_name='姓名')
    project = models.ForeignKey(Project, related_name='project_jobs', on_delete=models.CASCADE,
                                verbose_name='项目组')
    jobs_day = models.IntegerField('在岗天数', default=5, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.people

    class Meta:
        verbose_name = '公司人员在岗情况'
        verbose_name_plural = verbose_name


class PeopleGrouping(models.Model):
    project = models.ForeignKey(Project, related_name='project_grouping', on_delete=models.CASCADE,
                                verbose_name='项目组')
    people = models.ForeignKey(People, related_name='grouping_name', on_delete=models.CASCADE,
                               verbose_name='姓名')
    start_time = models.DateField('投入时间', default=timezone.now, null=True, blank=True)
    end_time = models.DateField('结束投入时间', default=timezone.now, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.project

    class Meta:
        verbose_name = '人员分组情况'
        verbose_name_plural = verbose_name


class PeopleEntrance(models.Model):
    people = models.ForeignKey(People, related_name='people_entrance', on_delete=models.CASCADE,
                               verbose_name='姓名')
    TYPE = (
        (0, '入场'),
        (1, '未入场'),
        (2, '闲置'),
        (3, '其他'),
    )
    entrance_type = models.SmallIntegerField('入场状态', choices=TYPE, default=0)
    team = models.ForeignKey(Team, related_name='entrance_team', on_delete=models.CASCADE,
                             verbose_name='人员实施所在团队')
    project = models.ForeignKey(Project, related_name='project_entrance', on_delete=models.CASCADE,
                                verbose_name='人员当前入场项目')
    entrance_note = models.CharField('当前项目组描述', max_length=500, null=True, blank=True)
    end_time = models.DateField('项目结束时间', default=timezone.now, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.people

    class Meta:
        verbose_name = '人员入场情况表'
        verbose_name_plural = verbose_name
    

class PeopleEntranceList(models.Model):
    people_entrance = models.ForeignKey(PeopleEntrance, related_name='people_entrance_list', 
                                        on_delete=models.CASCADE, verbose_name='人员入场情况详情')
    entrance_date = models.DateField('年份日期', default=timezone.now, null=True, blank=True)
    project = models.ForeignKey(Project, related_name='project_entrance_list', on_delete=models.CASCADE,
                                verbose_name='人员当月日期所在项目')
    
    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.people_entrance
    
    class Meta:
        verbose_name = '人员入场情况详情'
        verbose_name_plural = verbose_name


class PeopleIdle(models.Model):
    people = models.ForeignKey(People, related_name='people_idle', on_delete=models.CASCADE,
                               verbose_name='姓名')
    # idle_day = models.IntegerField('合计闲置天数', default=0)
    note = models.CharField('备注', max_length=500, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    def __str__(self):
        return '%s' % self.people

    class Meta:
        verbose_name = '人员闲置情况登记表'
        verbose_name_plural = verbose_name


class PeopleIdleList(models.Model):
    people_idle = models.ForeignKey(PeopleIdle, related_name='people_idle_list', on_delete=models.CASCADE,
                                    verbose_name='人员闲置情况')
    idle_date = models.DateField('年份日期', null=True, blank=True)
    idle_day = models.IntegerField('当月闲置天数', default=0, null=True, blank=True)

    created_at = models.DateTimeField('创建/导入时间', auto_now_add=True)
    modified_at = models.DateTimeField('修改时间', auto_now=True)

    class Meta:
        verbose_name = '闲置情况详情'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '%s' % self.idle_date
